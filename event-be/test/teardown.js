'use strict'

const { unlink, readdir } = require('fs/promises')
const { join } = require('path')

async function removeUploadedImages () {
  const files = await readdir(join(__dirname, '../public/'))
  files.filter(file => file.endsWith('.png')).forEach(
    async (png) => { await unlink(join(__dirname, '../public/', png)) })
}

removeUploadedImages()
  .then(() => console.log('Public directory cleaned!'))
