'use strict'

// This file contains code that we reuse
// between our tests.

const Fastify = require('fastify')
const fp = require('fastify-plugin')
const App = require('../app')
const { MongoClient } = require('mongodb')
const sinon = require('sinon')
const { afterEach } = require('tap')

afterEach(() => {
  sinon.restore()
})

const DB_URI = 'mongodb://localhost:27017/eventDb'

const client = new MongoClient(DB_URI, { w: 1 })

// Fill in this config with all the configurations
// needed for testing the application
function config () {
  return {
    token: {
      secret: 'mysecret'
    },
    db: {
      url: DB_URI
    }
  }
}

// automatically build and tear down our instance
function build (t) {
  const app = Fastify()

  // fastify-plugin ensures that all decorators
  // are exposed for testing purposes, this is
  // different from the production setup
  app.register(fp(App), config())

  // tear down our app after we are done
  t.teardown(app.close.bind(app))

  return app
}

async function authenticate (t) {
  const app = build(t)
  const res = await app.inject({
    url: '/accounts/auth',
    method: 'post',
    body: { username: 'daigo', password: 'apprendista' }
  })
  const body = JSON.parse(res.body)
  return `Bearer ${body.token}`
}

module.exports = {
  config,
  build,
  client,
  authenticate
}
