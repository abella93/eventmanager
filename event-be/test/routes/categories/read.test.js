'use strict'

const { test } = require('tap')
const { build } = require('../../helper')
const sinon = require('sinon')

test('As a regular user I want to display all categories', async function (t) {
  const app = build(t)
  const res = await app.inject({ url: '/categories', method: 'get' })
  const body = JSON.parse(res.body)

  // Assertions
  t.equal(res.statusCode, 200, 'It should return 200')
  t.ok(body.categories, 'Should contain categories properties')
  t.type(body.categories, 'Array', 'It should be an array')
})

test('If the category\'s list is empty it should return an error message', async function (t) {
  const app = await build(t)

  sinon.stub(app.Category, 'findCategories').callsFake(() => [])
  const res = await app.inject({ url: '/categories', method: 'get' })
  t.equal(res.statusCode, 404)
})

test('It should return a category with correct id', async function (t) {
  const app = build(t)
  const res = await app.inject({
    url: '/categories/61a110bc1e60d14113e48a28',
    method: 'get'
  })

  t.equal(res.statusCode, 200, 'Should return 200')

  const anotherRes = await app.inject({
    url: '/categories/61a110bc1e60d14113e48a2a',
    method: 'get'
  })

  t.equal(anotherRes.statusCode, 404, 'Should return not found')
})
