'use strict'

const { test } = require('tap')
const { build, authenticate } = require('../../helper')
const sinon = require('sinon')

test('As a registered user I want to create a category', async function (t) {
  const mockCategory = {
    name: 'film',
    slug: 'film101',
    description: 'cinema sotto le stelle',
    icon: 'ciak',
    owner: '61a10e75b55b1c6519a98b63'
  }
  const app = build(t)
  const token = await authenticate(t)
  const res = await app.inject({
    url: '/categories',
    method: 'post',
    headers: { authorization: token },
    body: mockCategory
  })
  const body = JSON.parse(res.body)

  // Assertions
  t.equal(res.statusCode, 200, 'It should return 200')
  t.ok(body._id, 'It Should return the created id of the category')
})

test('Should throw error if insertion fail', async function (t) {
  const mockCategory = {
    name: 'film',
    slug: 'film101',
    description: 'cinema sotto le stelle',
    icon: 'ciak',
    owner: '61a10e75b55b1c6519a98b63'
  }
  const app = await build(t)
  const token = await authenticate(t)
  sinon.stub(app.Category, 'insertCategory').callsFake(() => { throw new Error('Creation Fail') })
  const res = await app.inject({
    url: '/categories',
    method: 'post',
    headers: { authorization: token },
    body: mockCategory
  })

  // Assertions
  t.equal(res.statusCode, 500, 'It should return 500')
})
