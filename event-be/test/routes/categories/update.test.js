'use strict'

const { test } = require('tap')
const { build, authenticate } = require('../../helper')
const sinon = require('sinon')

test('As a registered user I want to change a category', async function (t) {
  const app = build(t)
  const token = await authenticate(t)
  const doc = { slug: 'music12dvfg', owner: '61a10e75b55b1c6519a98b63', name: 'musica' }
  const res = await app.inject({
    url: '/categories/61a110bc1e60d14113e48a28',
    method: 'put',
    headers: { authorization: token },
    body: doc
  })
  const body = JSON.parse(res.body)

  // Assertions
  t.equal(res.statusCode, 200, 'It should return 200')
  t.ok(body.count, 'Should contain count property')
  t.equal(body.count, 1, 'Should update one document only')
})

test('Should throw error if update fail', async function (t) {
  const app = await build(t)
  const token = await authenticate(t)
  const doc = { slug: 'music12dvfg', owner: '61a10e75b55b1c6519a98b63', name: 'musica' }
  sinon.stub(app.Category, 'updateCategory').callsFake(() => { throw new Error('Update Fail') })
  const res = await app.inject({
    url: '/categories/61a110bc1e60d14113e48a28',
    method: 'put',
    headers: { authorization: token },
    body: doc
  })
  t.equal(res.statusCode, 500)
})
