'use strict'

const { test } = require('tap')
const { build, authenticate } = require('../../helper')
const sinon = require('sinon')

test('As a registered user I want to create an event', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const event = {
    name: 'Esibizione Sotto i vulcano',
    description: 'Cover band dei litfiba',
    category: '61a110bc1e60d14113e48a28',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61a110bc1e60d14113e48a2a',
    isDraft: true,
    participants: 0,
    place: 'Via Roma',
    starting: '2022-11-13T20:20:39+00:00',
    duration: 1,
    isOnline: false,
    tags: ['musica', 'musica dal vivo', 'litfiba'],
    isPublic: true,
    link: 'http://event/details/12vnfgn',
    addresses: [
      'diego_avella@email.it',
      'macco@email.it',
      'tecchia@email.it'
    ]
  }
  const res = await app.inject({
    url: '/events',
    method: 'post',
    headers: {
      authorization: token
    },
    body: event
  })
  const body = JSON.parse(res.body)
  t.equal(res.statusCode, 200, 'Should return 200')
  t.ok(body._id, 'Should contain created event')
})

test('As a registered user I cannot create an event if the category assigned not exist', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const event = {
    name: 'Esibizione Sotto i vulcano',
    description: 'Cover band dei litfiba',
    category: '61a110bc1e60d14113e48a2f',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61a110bc1e60d14113e48a2a',
    isDraft: false,
    participants: 0,
    starting: '2021-11-13T20:20:39+00:00',
    duration: 1,
    place: 'Via Roma',
    isOnline: false,
    tags: ['musica', 'musica dal vivo', 'litfiba'],
    isPublic: true,
    link: 'http://event/details/12vnfgn',
    addresses: [
      'diego_avella@email.it',
      'macco@email.it',
      'tecchia@email.it'
    ]
  }
  const res = await app.inject({
    url: '/events',
    method: 'post',
    headers: {
      authorization: token
    },
    body: event
  })
  t.equal(res.statusCode, 500, 'Should return 500')
})

test('As a registered user I cannot create an event if the promoter assigned not exist', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const event = {
    name: 'Esibizione Sotto i vulcano',
    description: 'Cover band dei litfiba',
    category: '61a110bc1e60d14113e48a28',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61a110bc1e60d14113e48a2c',
    isDraft: false,
    participants: 0,
    place: 'Via Roma',
    starting: '2021-11-13T20:20:39+00:00',
    duration: 1,
    isOnline: false,
    tags: ['musica', 'musica dal vivo', 'litfiba'],
    isPublic: true,
    link: 'http://event/details/12vnfgn',
    addresses: [
      'diego_avella@email.it',
      'macco@email.it',
      'tecchia@email.it'
    ]
  }
  const res = await app.inject({
    url: '/events',
    method: 'post',
    headers: {
      authorization: token
    },
    body: event
  })
  const body = JSON.parse(res.body)
  t.equal(res.statusCode, 500, 'Should return 500')
  t.equal(body.message, 'Error: The promoter not exist')
})

test('Should send a message on insertion fail', async function (t) {
  const token = await authenticate(t)
  const app = await build(t)
  const event = {
    name: 'Esibizione Sotto i vulcano',
    description: 'Cover band dei litfiba',
    category: '61a110bc1e60d14113e48a28',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61a110bc1e60d14113e48a2a',
    isDraft: true,
    participants: 0,
    starting: '2022-11-13T20:20:39+00:00',
    place: 'Via Roma',
    duration: 1,
    isOnline: false,
    tags: ['musica', 'musica dal vivo', 'litfiba'],
    isPublic: true,
    link: 'http://event/details/12vnfgn',
    addresses: [
      'diego_avella@email.it',
      'macco@email.it',
      'tecchia@email.it'
    ]
  }

  sinon.stub(app.Event, 'createEvent').callsFake(() => { throw new Error('Creation fail') })
  const res = await app.inject({
    url: '/events',
    method: 'post',
    headers: {
      authorization: token
    },
    body: event
  })
  t.equal(res.statusCode, 500, 'Should return 500')
})
