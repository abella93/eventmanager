'use strict'

const { test } = require('tap')
const { build, authenticate } = require('../../helper')
const sinon = require('sinon')

test('As a registered user I should update my own event', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const event = {
    name: 'Concerto dei Clayvorants',
    category: '61a110bc1e60d14113e48a28',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61a110bc1e60d14113e48a2a',
    isDraft: false,
    participants: 10,
    place: 'Via Roma',
    starting: '2022-11-13T20:20:39+00:00',
    duration: 2,
    isOnline: false,
    tags: ['musica', 'musica dal vivo', 'iron maiden'],
    isPublic: true,
    link: 'http://event/details/12vnfgn',
    addresses: [
      'diego_avella@email.it',
      'macco@email.it',
      'tecchia@email.it',
      'angelo@email.it'
    ]
  }
  const res = await app.inject({
    url: '/events/61a4ba8b1094cff6bf5027b7',
    method: 'put',
    headers: {
      authorization: token
    },
    body: event
  })
  const body = JSON.parse(res.body)
  t.equal(res.statusCode, 200, 'Should return 200')
  t.ok(body.count, 'Should contain count property')
  t.equal(body.count, 1, 'Should update one event')
})

test('As a registered used i cannot update an event that isn\'t mine', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const event = {
    name: 'Concerto dei Clayvorants',
    category: '61a110bc1e60d14113e48a28',
    owner: '61a10e75b55b1c6519a98b64',
    promoter: '61a110bc1e60d14113e48a2c',
    isDraft: false,
    participants: 10,
    starting: '2022-11-13T20:20:39+00:00',
    place: 'Via Roma',
    duration: 2,
    isOnline: false,
    tags: ['musica', 'musica dal vivo', 'iron maiden'],
    isPublic: true,
    link: 'http://event/details/12vnfgn',
    addresses: [
      'diego_avella@email.it',
      'macco@email.it',
      'tecchia@email.it',
      'angelo@email.it'
    ]
  }
  const res = await app.inject({
    url: '/events/61a4ba8b1094cff6bf5027b7',
    method: 'put',
    headers: {
      authorization: token
    },
    body: event
  })

  t.equal(res.statusCode, 401, 'Should return 401')
})

test('Should send message on update fail', async function (t) {
  const token = await authenticate(t)
  const app = await build(t)
  const event = {
    name: 'Concerto dei Clayvorants',
    category: '61a110bc1e60d14113e48a28',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61a110bc1e60d14113e48a2a',
    place: 'Via Roma',
    isDraft: false,
    participants: 10,
    starting: '2022-11-13T20:20:39+00:00',
    duration: 2,
    isOnline: false,
    tags: ['musica', 'musica dal vivo', 'iron maiden'],
    isPublic: true,
    link: 'http://event/details/12vnfgn',
    addresses: [
      'diego_avella@email.it',
      'macco@email.it',
      'tecchia@email.it',
      'angelo@email.it'
    ]
  }

  sinon.stub(app.Event, 'updateEvent').callsFake(() => { throw new Error('Update fail') })
  const res = await app.inject({
    url: '/events/61a4ba8b1094cff6bf5027b7',
    method: 'put',
    headers: {
      authorization: token
    },
    body: event
  })
  t.equal(res.statusCode, 500, 'It should return 500')
})

test('As a registered user I cannot update an event if the category assigned not exist', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const event = {
    name: 'Esibizione Sotto i vulcano',
    description: 'Cover band dei litfiba',
    category: '61a110bc1e60d14113e48a2f',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61a110bc1e60d14113e48a2a',
    isDraft: false,
    participants: 0,
    place: 'Via Roma',
    starting: '2022-11-13T20:20:39+00:00',
    duration: 1,
    isOnline: false,
    tags: ['musica', 'musica dal vivo', 'litfiba'],
    isPublic: true,
    link: 'http://event/details/12vnfgn',
    addresses: [
      'diego_avella@email.it',
      'macco@email.it',
      'tecchia@email.it'
    ]
  }
  const res = await app.inject({
    url: '/events/61a4ba8b1094cff6bf5027b7',
    method: 'put',
    headers: {
      authorization: token
    },
    body: event
  })
  t.equal(res.statusCode, 500, 'Should return 500')
})

test('As a registered user I cannot update an event if the promoter assigned not exist', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const event = {
    name: 'Esibizione Sotto i vulcano',
    description: 'Cover band dei litfiba',
    category: '61a110bc1e60d14113e48a28',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61a110bc1e60d14113e48a2c',
    isDraft: false,
    participants: 0,
    place: 'Via Roma',
    starting: '2022-11-13T20:20:39+00:00',
    duration: 1,
    isOnline: false,
    tags: ['musica', 'musica dal vivo', 'litfiba'],
    isPublic: true,
    link: 'http://event/details/12vnfgn',
    addresses: [
      'diego_avella@email.it',
      'macco@email.it',
      'tecchia@email.it'
    ]
  }
  const res = await app.inject({
    url: '/events/61a4ba8b1094cff6bf5027b7',
    method: 'put',
    headers: {
      authorization: token
    },
    body: event
  })
  t.equal(res.statusCode, 500, 'Should return 500')
})
