'use strict'

const { test } = require('tap')
const { build, authenticate } = require('../../helper')

test('It should set participants 0 if a provate event doesn\'t have emails ', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const event = {
    name: 'Aerosmith in tour 2022',
    description: 'Aerosmith in tour',
    category: '61a110bc1e60d14113e48a28',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61a110bc1e60d14113e48a2a',
    isDraft: false,
    place: 'Via Roma',
    starting: '2022-11-13T20:20:39+00:00',
    duration: 1,
    isOnline: false,
    tags: ['musica', 'musica dal vivo', 'negramaro'],
    isPublic: false,
    link: 'http://event/details/12vnfgn'
  }
  const res = await app.inject({
    url: '/events',
    method: 'post',
    headers: {
      authorization: token
    },
    body: event
  })

  t.equal(res.statusCode, 200, 'Should return 200')
  const body = JSON.parse(res.body)
  const anotherRes = await app.inject({
    url: '/events/' + body._id,
    method: 'get'
  })
  const anotherBody = JSON.parse(anotherRes.body)
  t.equal(anotherBody.participants, 0, 'Should have 0 participants')
})

test('on update, it should set participants 0 if a private event doesn\'t have emails ', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const event = {
    name: 'Evento webeetle',
    description: 'Evento organizzato da webeetle',
    category: '61a110bc1e60d14113e48a29',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61b33890fc13ae2170000202',
    starting: '2022-11-13T20:20:39+00:00',
    isDraft: true,
    isPublic: false,
    isOnline: false,
    place: '6076 Mandrake Street',
    link: 'https:/webeetle.it',
    participants: 3,
    tags: [
      'Asoka',
      'Toughjoyfax',
      'Rank'
    ]
  }

  const res = await app.inject({
    url: '/events/61b33243fc13ae6971000337',
    method: 'put',
    headers: {
      authorization: token
    },
    body: event
  })
  t.equal(res.statusCode, 200, 'Should return 200')
  const anotherRes = await app.inject({
    url: '/events/61b33243fc13ae6971000337',
    method: 'get'
  })
  const body = JSON.parse(anotherRes.body)
  t.equal(body.participants, 0, 'Should have 0 participants')
})

test('on update, it should set participants if a private event has emails ', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const event = {
    name: 'Evento webeetle',
    description: 'Evento organizzato da webeetle',
    category: '61a110bc1e60d14113e48a29',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61b33890fc13ae2170000202',
    starting: '2022-11-13T20:20:39+00:00',
    isDraft: true,
    isPublic: false,
    isOnline: false,
    place: '6076 Mandrake Street',
    link: 'https:/webeetle.it',
    participants: 3,
    tags: [
      'Asoka',
      'Toughjoyfax',
      'Rank'
    ],
    addresses: [
      'email1@email.it',
      'email2@email.com'
    ]
  }

  const res = await app.inject({
    url: '/events/61b33243fc13ae6971000337',
    method: 'put',
    headers: {
      authorization: token
    },
    body: event
  })
  t.equal(res.statusCode, 200, 'Should return 200')
  const anotherRes = await app.inject({
    url: '/events/61b33243fc13ae6971000337',
    method: 'get'
  })
  const body = JSON.parse(anotherRes.body)
  t.equal(body.participants, 2, 'Should have 2 participants')
})
