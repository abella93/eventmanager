'use strict'

const { test } = require('tap')
const { build, authenticate } = require('../../helper')

test('It should send email on private events with success', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const event = {
    name: 'Negramaro in tour 2021',
    description: 'Concerto dei negramaro',
    category: '61a110bc1e60d14113e48a28',
    owner: '61a10e75b55b1c6519a98b63',
    promoter: '61a110bc1e60d14113e48a2a',
    isDraft: false,
    place: 'Via Roma',
    participants: 0,
    starting: '2022-11-13T20:20:39+00:00',
    duration: 1,
    isOnline: false,
    tags: ['musica', 'musica dal vivo', 'negramaro'],
    isPublic: false,
    link: 'http://event/details/12vnfgn',
    addresses: [
      'diego_avella@email.it',
      'macco@email.it',
      'tecchia@email.it'
    ]
  }
  const res = await app.inject({
    url: '/events',
    method: 'post',
    headers: {
      authorization: token
    },
    body: event
  })

  t.equal(res.statusCode, 200, 'Should return 200')
})
