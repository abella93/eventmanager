'use strict'

const { test } = require('tap')
const { build, authenticate } = require('../../helper')
const formAutoContent = require('form-auto-content')
const { createReadStream } = require('fs')
const { join } = require('path')

test('It should upload an image with success', async function (t) {
  const token = await authenticate(t)
  const app = build(t)
  const formData = formAutoContent({
    _id: '61a4ba8b1094cff6bf5027b7',
    cover: createReadStream(join(__dirname, '../../fixtures/test.png'))
  })
  formData.headers.Authorization = token
  const res = await app.inject({
    url: '/events/upload',
    method: 'post',
    ...formData
  })
  const body = JSON.parse(res.body)
  t.equal(res.statusCode, 200, 'Should return 200')
  t.equal(body.count, 1, 'Should be updated only one event')
})
