'use strict'

const { test } = require('tap')
const { build, authenticate } = require('../../helper')
const sinon = require('sinon')

test('As a user I want to view only published events', async function (t) {
  const app = build(t)
  const res = await app.inject({
    url: '/events/history',
    method: 'get'
  })

  const body = JSON.parse(res.body)
  t.equal(res.statusCode, 200, 'It should return 200')
  t.ok(body.events, 'Should contain event list')
})

test('As a authenticated user I want to retrieve all events', async function (t) {
  const token = await authenticate(t)
  const app = await build(t)
  const res = await app.inject({
    url: '/events',
    method: 'get',
    headers: {
      authorization: token
    }
  })
  t.equal(res.statusCode, 200, 'Should return 200')
})

test('As a user search by name', async function (t) {
  const app = build(t)
  const res = await app.inject({
    url: '/events/history?name=clayvorants',
    method: 'get'
  })
  const body = JSON.parse(res.body)
  t.equal(res.statusCode, 200, 'It should return 200')
  t.ok(body.events, 'Should contain event list')
})

test('It should send an error if events are empty', async function (t) {
  const app = await build(t)
  sinon.stub(app.Event, 'findEvents').callsFake(() => [])
  const token = await authenticate(t)
  const res = await app.inject({
    url: '/events',
    method: 'get',
    headers: {
      authorization: token
    }
  })
  t.equal(res.statusCode, 404)
})

test('It should return an event with correct id', async function (t) {
  const app = build(t)
  const res = await app.inject({
    url: '/events/61a4ba8b1094cff6bf5027b7',
    method: 'get'
  })

  t.equal(res.statusCode, 200, 'Should return 200')

  const anotherRes = await app.inject({
    url: '/events/61a110bc1e60d14113e48a2d',
    method: 'get'
  })

  t.equal(anotherRes.statusCode, 404, 'Should return not found')
})
