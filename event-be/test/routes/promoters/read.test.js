'use strict'

const { test } = require('tap')
const { build } = require('../../helper')
const sinon = require('sinon')

test('As a normal user I want to display all promoters', async function (t) {
  const app = build(t)
  const res = await app.inject({
    url: '/promoters',
    method: 'get'
  })
  const body = JSON.parse(res.body)

  // Asserions
  t.ok(body.promoters, 'Should contain promoter property')
  t.equal(res.statusCode, 200, 'It should return 200')
  t.type(body.promoters, 'Array', 'promoters should be an array')
})

test('When promoters are empty it should return an error code', async function (t) {
  const app = await build(t)
  sinon.stub(app.Promoter, 'findPromoters').callsFake(() => [])
  const res = await app.inject({ url: '/promoters', method: 'get' })
  t.equal(res.statusCode, 404)
})

test('It should return a promoter with correct id', async function (t) {
  const app = build(t)
  const res = await app.inject({
    url: '/promoters/61a110bc1e60d14113e48a2a',
    method: 'get'
  })

  t.equal(res.statusCode, 200, 'Should return 200')

  const anotherRes = await app.inject({
    url: '/promoters/61a110bc1e60d14113e48a2d',
    method: 'get'
  })

  t.equal(anotherRes.statusCode, 404, 'Should return not found')
})
