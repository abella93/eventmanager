'use strict'

const { test } = require('tap')
const { authenticate, build } = require('../../helper')
const sinon = require('sinon')

test('As a registered user I want to edit a promoter', async function (t) {
  const token = await authenticate(t)
  // TODO missing body
  const app = build(t)
  const promoter = {
    email: 'libriamoci@example.it',
    owner: '61a10e75b55b1c6519a98b63',
    forename: 'MacGyver, Hamill and Welch'
  }
  const res = await app.inject({
    url: '/promoters/61a110bc1e60d14113e48a2b',
    method: 'put',
    headers: {
      authorization: token
    },
    body: promoter
  })
  const body = JSON.parse(res.body)
  // Assertions
  t.equal(res.statusCode, 200, 'It should return 200')
  t.ok(body.count, 'It should contain count property')
  t.equal(body.count, 1, 'Should update one promoter only')
})

test('Should send an error if update fail', async function (t) {
  const token = await authenticate(t)
  const app = await build(t)
  sinon.stub(app.Promoter, 'updatePromoter').callsFake(() => { throw new Error('Update fail') })
  const promoter = {
    email: 'libriamoci@example.it',
    owner: '61a10e75b55b1c6519a98b63',
    forename: 'MacGyver, Hamill and Welch'
  }
  const res = await app.inject({
    url: '/promoters/61a110bc1e60d14113e48a2b',
    method: 'put',
    headers: {
      authorization: token
    },
    body: promoter
  })

  // Assertions
  t.equal(res.statusCode, 500, 'It should return 500')
})
