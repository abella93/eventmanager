'use strict'

const { test } = require('tap')
const { authenticate, build } = require('../../helper')
const sinon = require('sinon')

test('As a registered user I want to create a promoter', async function (t) {
  const token = await authenticate(t)
  const fakePromoter = {
    forename: 'la fenice',
    email: 'lafenice@example.com',
    description: 'organizza film all\'aperto',
    website: 'http://www.lafenice.it',
    icon: 'http://www.dummyicon.it',
    owner: '61a10e75b55b1c6519a98b63'
  }

  const app = build(t)
  const res = await app.inject({
    url: '/promoters',
    method: 'post',
    headers: {
      authorization: token
    },
    body: fakePromoter
  })
  const body = JSON.parse(res.body)

  // Assertions
  t.equal(res.statusCode, 200, 'It should return 200')
  t.ok(body._id, 'Should return the created promoter')
})

test('Should send an error if insertion fail', async function (t) {
  const token = await authenticate(t)
  const fakePromoter = {
    forename: 'la fenice',
    email: 'lafenice@example.com',
    description: 'organizza film all\'aperto',
    website: 'http://www.lafenice.it',
    icon: 'http://www.dummyicon.it',
    owner: '61a10e75b55b1c6519a98b63'
  }

  const app = await build(t)

  sinon.stub(app.Promoter, 'insertPromoter').callsFake(() => { throw new Error('Insertion fail') })
  const res = await app.inject({
    url: '/promoters',
    method: 'post',
    headers: {
      authorization: token
    },
    body: fakePromoter
  })

  // Assertions
  t.equal(res.statusCode, 500, 'It should return 500')
})
