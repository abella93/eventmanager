'use strict'

const { test } = require('tap')
const { build, authenticate } = require('../../helper')

test('As a logged user I want to know my roles', async function (t) {
  const app = build(t)
  const token = await authenticate(t)
  const res = await app.inject({
    method: 'post',
    url: '/accounts/authorize',
    headers: { authorization: token }
  })
  const body = JSON.parse(res.body)
  t.ok(body.role, 'Should contain role')
  t.ok(body.id, 'Should contain id')
})

test('As a normal user I cannot access private routes', async function (t) {
  const app = build(t)
  const mockCategory = {
    name: 'film',
    slug: 'film101',
    description: 'cinema sotto le stelle',
    icon: 'ciak'
  }
  const res = await app.inject({
    url: '/categories',
    method: 'post',
    body: mockCategory
  })
  t.equal(res.statusCode, 401, 'Should return unauthorized')
})
