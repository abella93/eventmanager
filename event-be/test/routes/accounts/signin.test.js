'use strict'

const { test } = require('tap')
const { build } = require('../../helper')

test('As a user I want to login into the system with username and password', async function (t) {
  const app = build(t)
  const res = await app.inject({
    url: '/accounts/auth',
    method: 'post',
    body: {
      username: 'daigo',
      password: 'apprendista'
    }
  })

  const body = JSON.parse(res.body)

  // Assertions
  t.equal(res.statusCode, 200, 'Should return 200')
  t.ok(body.token, 'It should return a jwt')
})

test('As a user I want to fail login if username or password are wrong', async function (t) {
  const app = build(t)
  const res = await app.inject({
    url: '/accounts/auth',
    method: 'post',
    body: {
      username: 'notregistered',
      password: 'apprendista'
    }
  })
  const body = JSON.parse(res.body)

  // Assertions
  t.equal(res.statusCode, 404, 'It should return not found code')
  t.equal(body.message, 'Account non trovato', 'It should say that the account is not found')
})
