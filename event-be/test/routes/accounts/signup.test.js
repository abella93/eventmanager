'use strict'

const { test } = require('tap')
const { build } = require('../../helper')
const sinon = require('sinon')

test('As a user i want to signup to the system', async function (t) {
  const fakeAccount = { name: 'Davide', username: 'david', password: 'analista' }

  const app = build(t)
  let res = await app.inject({
    url: '/accounts',
    method: 'post',
    body: fakeAccount
  })

  let body = JSON.parse(res.body)
  t.equal(res.statusCode, 200, 'Should be 200')
  t.ok(body._id, 'Should contain identification number')

  res = await app.inject({
    url: '/accounts',
    method: 'post',
    body: fakeAccount
  })

  body = JSON.parse(res.body)

  // Assertions
  t.equal(res.statusCode, 400, 'Should return 400')
  t.equal(body.message, 'Account già presente', 'Should send message error')
})

test('Insert should fail on other error type', async function (t) {
  const app = await build(t)
  const fakeAccount = {
    name: 'Paolo',
    email: 'paolo@email.it',
    username: 'paolus',
    password: 'analista'
  }
  sinon.stub(app.Account, 'insertAccount').callsFake(() => { throw new Error('Insert error') })
  const res = await app.inject({
    url: '/accounts',
    method: 'post',
    body: fakeAccount
  })
  t.equal(res.statusCode, 500, 'Should return 500')
})
