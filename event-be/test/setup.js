'use strict'

const { client } = require('./helper')
const { join } = require('path')
const { exit } = require('process')
const clean = require('mongo-clean')
const { ObjectID } = require('bson')

async function importData (json, collection) {
  const dump = require(join(__dirname, 'fixtures', json))
  const transformedDump = dump.map(el => {
    el._id = new ObjectID(el._id)
    return el
  })
  await client.db('eventDb').collection(collection).insertMany(transformedDump)
}

async function populateDatabase () {
  await client.connect()
  await clean(client.db('eventDb'))
  await importData('event.json', 'events')
  await importData('account.json', 'accounts')
  await importData('category.json', 'categories')
  await importData('promoter.json', 'promoters')
}

populateDatabase().then(() => {
  console.log('Migration complete')
  exit(0)
})
