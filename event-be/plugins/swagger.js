const fp = require('fastify-plugin')
const swagger = require('fastify-swagger')

module.exports = fp(async function (fastify, opts) {
  fastify.register(swagger, {
    routePrefix: '/',
    openapi: {
      info: {
        title: 'Event API',
        description: 'Testing API for the Event',
        version: '1.0.0'
      },
      servers: [{
        url: 'http://localhost:3000',
        description: 'Development Server',
        port: {
          default: 3000
        }
      }],
      components: {
        securitySchemes: {
          Bearer: {
            type: 'http',
            scheme: 'bearer',
            description: 'Authorization with JWT',
            in: 'header',
            bearerFormat: 'JWT'
          }
        }
      },
      consumes: ['application/json'],
      produces: ['application/json'],
      tags: [
        { name: 'account', description: 'Accounts\'s endpoints' },
        { name: 'events', description: 'Events\'s endpoints' },
        { name: 'categories', description: 'categories\'s endpoint' },
        { name: 'promoters', description: 'promoters\'s endpoint' }
      ]
    },
    hideUntagged: true,
    exposeRoute: true
  })
})
