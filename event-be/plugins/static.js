'use strict'

const fp = require('fastify-plugin')
const assets = require('fastify-static')
const { join } = require('path')

module.exports = fp(async function (fastify, opts) {
  fastify.register(assets, {
    root: join(__dirname, '..', 'public'),
    prefix: '/public/'
  })
})
