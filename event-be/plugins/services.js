const fp = require('fastify-plugin')
const { AccountRepository } = require('../routes/accounts/repository')
const { EventRepository } = require('../routes/events/repository')
const { PromoterRepository } = require('../routes/promoters/repository')
const { CategoryRepository } = require('../routes/categories/repository')

module.exports = fp(async function (fastify, opts) {
  fastify.decorate('Account', await AccountRepository(fastify.mongo))

  fastify.decorate('Category', await CategoryRepository(fastify.mongo))

  fastify.decorate('Event', await EventRepository(fastify.mongo))

  fastify.decorate('Promoter', await PromoterRepository(fastify.mongo))
})
