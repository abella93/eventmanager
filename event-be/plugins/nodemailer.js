const fp = require('fastify-plugin')
const mailer = require('fastify-mailer')

module.exports = fp(async function (fastify, opts) {
  fastify.register(mailer, {
    defaults: { from: 'EventUI <eventUi@gmail.com>' },
    transport: {
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT
    }
  })
})
