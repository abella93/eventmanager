'use strict'

const fp = require('fastify-plugin')
const jwt = require('fastify-jwt')

module.exports = fp(async function (fastify, opts) {
  fastify.register(jwt, Object.assign({}, {
    secret: process.env.JWT_SECRET
  }, opts.token))

  fastify.decorate('authorize', async function (req, res) {
    try {
      await req.jwtVerify()
    } catch (error) {
      res.send(error)
    }
  })
})
