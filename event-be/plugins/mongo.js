'use strict'

const fp = require('fastify-plugin')
const mongo = require('fastify-mongodb')

module.exports = fp(async function (fastify, opts) {
  fastify.register(mongo, Object.assign({}, {
    forceClose: true,
    url: `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DBNAME}`
  }, opts.db))
})
