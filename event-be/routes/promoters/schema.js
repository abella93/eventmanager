const S = require('fluent-json-schema')
const { errorObject, updateResponse } = require('../../common/schemas')

const promoter = S.object()
  .additionalProperties(false)
  .id('#promoter')
  .prop('forename', S.string().minLength(1).maxLength(30).required())
  .prop('email', S.string().format(S.FORMATS.EMAIL).required())
  .prop('description', S.string().maxLength(255))
  .prop('website', S.string().format(S.FORMATS.URI).maxLength(80))
  .prop('owner', S.string().required())

const createPromoterSchema = {
  description: 'Create a new Promoter',
  security: [
    { Bearer: [] }
  ],
  tags: ['promoters'],
  body: promoter,
  response: {
    200: { type: 'object', properties: { _id: { type: 'string' } } },
    '4xx': errorObject
  }
}

const updatePromoterSchema = {
  description: 'Update an existing promoter',
  security: [
    { Bearer: [] }
  ],
  tags: ['promoters'],
  required: ['id'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'string' }
    }
  },
  body: promoter,
  response: {
    200: updateResponse,
    500: errorObject
  }
}

const findPromotersSchema = {
  schema: {
    description: 'Find all promoters in the system',
    tags: ['promoters'],
    response: {
      200: S.object().prop('promoters', S.array().items(promoter.prop('_id', S.string()))),
      404: errorObject
    }
  }
}

const findPromoterSchema = {
  schema: {
    description: 'Find a single promoter',
    tags: ['promoters'],
    required: ['id'],
    params: {
      type: 'object',
      properties: {
        id: {
          type: 'string'
        }
      }
    },
    response: {
      200: promoter.prop('_id', S.string()),
      404: errorObject
    }
  }
}

module.exports = {
  promoter,
  createPromoterSchema,
  findPromotersSchema,
  updatePromoterSchema,
  findPromoterSchema
}
