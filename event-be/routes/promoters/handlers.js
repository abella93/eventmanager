'use strict'

/**
 * Create a new promoter
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 */
async function createPromoter (req, res) {
  return await this.Promoter.insertPromoter(req.body)
}

/**
 * Update an existing promoter
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 */
async function updatePromoter (req, res) {
  try {
    return await this.Promoter.updatePromoter(req.params.id, req.body)
  } catch (error) {
    return res.internalServerError(error)
  }
}

async function findPromoters (req, res) {
  const promoterList = await this.Promoter.findPromoters()
  return promoterList.length === 0
    ? res.notFound('Non ci sono organizzatori')
    : { promoters: promoterList }
}

async function findPromoter (req, res) {
  const promoter = await this.Promoter.findPromoterById(req.params.id)
  return promoter || res.notFound('Organizzatore non trovato')
}

module.exports = { createPromoter, updatePromoter, findPromoters, findPromoter }
