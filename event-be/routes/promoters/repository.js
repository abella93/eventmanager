'use strict'

const { ObjectID } = require('bson')

/**
 *
 * @param {import('mongodb').MongoClient} storage the mongo db storage
 */
async function PromoterRepository (storage) {
  /**
     * @type {import('mongodb').Collection} promoters
     */
  const promoters = storage.db.collection('promoters')

  return {
    /**
         * Insert a new promoter into the collection
         * @param {Object} promoter the promoter
         * @returns The id of the created promoted
         */
    async insertPromoter (promoter) {
      const createdPromoter = await promoters.insertOne(promoter)
      return { _id: createdPromoter.insertedId.toString() }
    },

    /**
         * Update a specific promoter
         * @param {string} id the id of the promoter
         * @param {Object} promoter the promoter updated records
         */
    async updatePromoter (id, promoter) {
      const updatedPromoter = await promoters.updateOne(
        { _id: new ObjectID(id) },
        { $set: promoter })
      return { count: updatedPromoter.modifiedCount }
    },

    /**
         * Get a list of promoters
         * @returns An array of promoter object
         */
    async findPromoters () {
      return await promoters.find().toArray()
    },

    /**
         * Find a specific promoter by id
         * @param {string} id the id of the promotrr
         */
    async findPromoterById (id) {
      return await promoters.findOne({ _id: new ObjectID(id) })
    }
  }
}

module.exports = { PromoterRepository }
