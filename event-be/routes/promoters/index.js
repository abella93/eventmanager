'use strict'

const {
  createPromoter,
  updatePromoter,
  findPromoters,
  findPromoter
} = require('./handlers')

const {
  createPromoterSchema,
  findPromotersSchema,
  updatePromoterSchema,
  findPromoterSchema
} = require('./schema')

const { assertOwnership } = require('../../common/hooks')

/**
 * Gather all endpoints from the promoters resource
 * @param {import('fastify').FastifyInstance} fastify app
 * @param {Object} opts envionment options
 */
module.exports = async function (fastify, opts) {
  fastify.get('/', findPromotersSchema, findPromoters)

  fastify.post('/', {
    preValidation: [fastify.authorize],
    schema: createPromoterSchema
  }, createPromoter)

  fastify.put('/:id', {
    schema: updatePromoterSchema,
    preValidation: [fastify.authorize, assertOwnership]
  }, updatePromoter)

  fastify.get('/:id', findPromoterSchema, findPromoter)
}
