/**
 * Create a new category
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 */
async function createCategory (req, res) {
  try {
    return await this.Category.insertCategory(req.body)
  } catch (error) {
    return res.internalServerError()
  }
}

/**
 * Create a new category
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 */
async function updateCategory (req, res) {
  try {
    return await this.Category.updateCategory(req.params.id, req.body)
  } catch (error) {
    return res.internalServerError(error)
  }
}

/**
 * Find categories
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 */
async function findCategories (req, res) {
  const categoryList = await this.Category.findCategories()
  return categoryList.length === 0
    ? res.notFound('Non esistono categorie')
    : { categories: categoryList }
}

async function findCategory (req, res) {
  const category = await this.Category.findCategoryById(req.params.id)
  return category || res.notFound('Categoria non trovata')
}

module.exports = { createCategory, updateCategory, findCategories, findCategory }
