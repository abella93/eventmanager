'use strict'

const { ObjectID } = require('bson')

/**
 * Create a repository to access the category data model
 * @param {import('mongodb').MongoClient} storage the mongodb storage
 */
async function CategoryRepository (storage) {
  /**
   * @type {import('mongodb').Collection} categories
   */
  const categories = storage.db.collection('categories')

  return {
    /**
     * Create a new category
     * @param {Object} category the new category
     */
    async insertCategory (category) {
      const createdCategory = await categories.insertOne(category)
      return { _id: createdCategory.insertedId.toString() }
    },

    /**
     * Update an existing category
     * @param {string} id the id of the existing category
     * @param {Object} category the updated records of the category
     */
    async updateCategory (id, category) {
      const updatedCategory = await categories.updateOne({ _id: new ObjectID(id) }, { $set: category })
      return { count: updatedCategory.modifiedCount }
    },

    /**
     * Find all categories
     */
    async findCategories () {
      return await categories.find().toArray()
    },

    /**
     * Find a category by its id
     * @param {string} id the id of the existing category
     */
    async findCategoryById (id) {
      return await categories.findOne({ _id: new ObjectID(id) })
    }
  }
}

module.exports = { CategoryRepository }
