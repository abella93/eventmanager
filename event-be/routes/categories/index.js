'use strict'

const {
  createCategory, updateCategory,
  findCategories, findCategory
} = require('./handlers')

const {
  createCategorySchema, updateCategorySchema,
  findCategoriesSchema, findCategorySchema
} = require('./schema')

const { assertOwnership } = require('../../common/hooks')

/**
 * Gather all endpoints from the category resource
 * @param {import('fastify').FastifyInstance} fastify app
 * @param {Object} opts envionment options
 */
module.exports = async function (fastify, opts) {
  fastify.get('/', findCategoriesSchema, findCategories)

  fastify.post('/', {
    preValidation: [fastify.authorize],
    schema: createCategorySchema
  }, createCategory)

  fastify.put('/:id', {
    preValidation: [fastify.authorize, assertOwnership],
    schema: updateCategorySchema
  }, updateCategory)

  fastify.get('/:id', findCategorySchema, findCategory)
}
