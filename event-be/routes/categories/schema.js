const S = require('fluent-json-schema')
const { errorObject, updateResponse } = require('../../common/schemas')

const category = S.object()
  .additionalProperties(false)
  .id('#category')
  .prop('name', S.string().minLength(1).required())
  .prop('slug', S.string())
  .prop('description', S.string())
  .prop('owner', S.string().required())

const createCategorySchema = {
  description: 'Create a new category',
  security: [
    { Bearer: [] }
  ],
  tags: ['categories'],
  body: category,
  responses: {
    200: {
      type: 'object',
      properties: {
        _id: { type: 'string' }
      }
    },
    400: errorObject,
    500: errorObject
  }
}

const updateCategorySchema = {
  description: 'update a category',
  security: [
    { Bearer: [] }
  ],
  tags: ['categories'],
  required: ['id'],
  params: {
    type: 'object',
    properties: {
      id: {
        type: 'string'
      }
    }
  },
  body: category,
  response: {
    200: updateResponse,
    400: errorObject,
    500: errorObject
  }
}

const findCategoriesSchema = {
  schema: {
    description: 'Find all categories in the system',
    tags: ['categories'],
    reponses: {
      200: S.object().prop('categories', S.array().items(category.prop('_id', S.string()))),
      404: errorObject
    }
  }
}

const findCategorySchema = {
  schema: {
    description: 'Find a specific category',
    tags: ['categories'],
    required: ['id'],
    params: {
      type: 'object',
      properties: {
        id: {
          type: 'string'
        }
      }
    },
    responses: {
      200: category.prop('_id', S.string()),
      404: errorObject
    }
  }
}

module.exports = {
  category,
  createCategorySchema,
  updateCategorySchema,
  findCategoriesSchema,
  findCategorySchema
}
