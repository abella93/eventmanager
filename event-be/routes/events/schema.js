const S = require('fluent-json-schema')
const { errorObject, updateResponse } = require('../../common/schemas')
const { category } = require('../categories/schema')

const event = S.object()
  .additionalProperties(false)
  .id('#event')
  .prop('name', S.string().minLength(1).required())
  .prop('description', S.string().maxLength(255))
  .prop('category', S.string().required())
  .prop('owner', S.string().required())
  .prop('promoter', S.string().required())
  .prop('isDraft', S.boolean().required())
  .prop('participants', S.number())
  .prop('starting', S.string().format(S.FORMATS.DATE_TIME).required())
  .prop('duration', S.number())
  .prop('isOnline', S.boolean())
  .prop('tags', S.array().items(S.string()))
  .prop('place', S.string().required())
  .prop('isPublic', S.boolean().required())
  .prop('link', S.string().format(S.FORMATS.URI))
  .prop('addresses', S.array().items(S.string().format(S.FORMATS.EMAIL)))
  .prop('cover', S.string())

const createEventSchema = {
  description: 'Create a new event',
  security: [
    { Bearer: [] }
  ],
  tags: ['events'],
  body: event,
  response: {
    200: {
      type: 'object',
      properties: {
        _id: { type: 'string' }
      }
    },
    500: errorObject
  }
}

const findEventsSchema = {
  description: 'Find all events',
  tags: ['events'],
  security: [
    { Bearer: [] }
  ],
  response: {
    200: S.object().prop('events', S.array().items(event.prop('_id', S.string()))),
    404: errorObject,
    500: errorObject
  }
}

const updateEventSchema = {
  description: 'Update an existing event',
  security: [
    { Bearer: [] }
  ],
  tags: ['events'],
  params: {
    type: 'object',
    properties: {
      id: { type: 'string' }
    }
  },
  body: event,
  required: ['id'],
  response: {
    200: updateResponse
  }
}

const findEventSchema = {
  schema: {
    description: 'Find a single event',
    tags: ['events'],
    params: {
      type: 'object',
      properties: {
        id: { type: 'string' }
      }
    },
    response: {
      200: event.prop('_id', S.string()),
      404: errorObject
    }
  }
}

const searchEvent = event.only([
  'name',
  'description',
  'category',
  'participants',
  'starting',
  'isOnline',
  'isPublic',
  'place',
  'link'
]).prop('_id', S.string()).prop('ct', S.array().items(category))

const searchEventSchema = {
  schema: {
    description: 'Search events',
    tags: ['events'],
    querystring: {
      type: 'object',
      properties: {
        name: {
          type: 'string'
        }
      }
    },
    200: S.object().prop('events', S.array().items(searchEvent)),
    404: errorObject
  }
}

const uploadSchema = {
  description: 'Upload an image',
  security: [
    { Bearer: [] }
  ],
  consumes: ['multipart/form-data'],
  body: {
    type: 'object',
    required: ['_id', 'cover'],
    properties: {
      _id: {
        properties: {
          value: {
            type: 'string'
          }
        }
      },
      cover: { $ref: '#mySharedSchema' }
    }
  },
  response: {
    200: updateResponse,
    '5xx': errorObject,
    '4xx': errorObject
  }
}

module.exports = {
  event,
  createEventSchema,
  findEventsSchema,
  updateEventSchema,
  findEventSchema,
  searchEventSchema,
  uploadSchema
}
