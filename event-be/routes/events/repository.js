'use strict'

const { ObjectID } = require('bson')

/**
 *
 * @param {import('mongodb').MongoClient} storage mongo instance
 */
async function EventRepository (storage) {
  /**
     * @type {import('mongodb').Collection} events
     */
  const events = storage.db.collection('events')
  await events.createIndex({ name: 'text' })

  return {
    /**
         * create a new event
         * @param {Object} event the event
         */
    async createEvent (event) {
      if (!event.isPublic) {
        event.participants = event.addresses ? event.addresses.length : 0
      }
      const createdEvent = await events.insertOne(event)
      return { _id: createdEvent.insertedId.toString() }
    },

    /**
         * Update a single event
         * @param {string} id the id of the event
         * @param {string} owner owner id of the event
         * @param {Object} event the event records
         * @returns The updated count of event
         */
    async updateEvent (id, owner, event) {
      if (!event.isPublic) {
        event.participants = event.addresses ? event.addresses.length : 0
      }
      const updatedEvent = await events.updateOne({ _id: new ObjectID(id), owner: owner }, { $set: event })
      return { count: updatedEvent.modifiedCount }
    },

    /**
         * Find all events
         * @returns Array of event objects
         */
    async findEvents () {
      return await events.find().toArray()
    },

    /**
         * Find a specific event by its id
         * @param {string} id the id of the event
         */
    async findEventById (id) {
      return await events.findOne({ _id: new ObjectID(id) })
    },

    /**
         * Search for public events
         * @param {Object} filters type of filters
         * @returns a list of event joined with its category
         */
    async searchEvents (filters) {
      return await events.aggregate(filters).toArray()
    }
  }
}

module.exports = { EventRepository }
