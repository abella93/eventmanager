const Mustache = require('mustache')
const { readFile } = require('fs/promises')
const { join } = require('path')

/**
 * Send emails to a series if web address passed by interface
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 */
async function sendEmails (req, res) {
  try {
    // check if the event is private and addresses are sent
    if (!req.body.isPublic && req.body.addresses && req.body.addresses.length !== 0) {
      for (const email of req.body.addresses) {
        await this.mailer.sendMail({
          to: email,
          subject: `Invito all'evento per ${email}`,
          html: Mustache.render(await readFile(join(__dirname, '../../template', 'email.html'),
            { encoding: 'utf-8' }), req.body)
        })
      }
    } else {
      throw new Error('the event must be reserved and at least one email required')
    }
  } catch (error) {
    return res.badRequest(error)
  }
}

module.exports = { sendEmails }
