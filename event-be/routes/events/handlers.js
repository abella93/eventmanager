const { writeFile } = require('fs/promises')
const { join } = require('path')

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 * @returns
 */
async function createEvent (req, res) {
  try {
    // handle category existance
    const category = await this.Category.findCategoryById(req.body.category)

    if (!category) {
      throw new Error('The category does not exist')
    }

    // handle promoter existance
    const promoter = await this.Promoter.findPromoterById(req.body.promoter)
    if (!promoter) {
      throw new Error('The promoter not exist')
    }

    return await this.Event.createEvent(req.body)
  } catch (error) {
    return res.internalServerError(error)
  }
}

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 * @returns
 */
async function updateEvent (req, res) {
  try {
    // handle category existance
    const category = await this.Category.findCategoryById(req.body.category)

    if (!category) {
      throw new Error('The category does not exist')
    }

    // handle promoter existance
    const promoter = await this.Promoter.findPromoterById(req.body.promoter)

    if (!promoter) {
      throw new Error('The promoter not exist')
    }

    return await this.Event.updateEvent(req.params.id, req.user.id, req.body)
  } catch (error) {
    return res.internalServerError(error)
  }
}

async function showEvents (req, res) {
  const eventList = await this.Event.findEvents()
  return eventList.length === 0 ? res.notFound('Nessun Evento al momento') : { events: eventList }
}

async function findEvent (req, res) {
  const event = await this.Event.findEventById(req.params.id)
  return event || res.notFound('Evento non trovato')
}

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 */
async function searchEvents (req, res) {
  const filter = [
    { $addFields: { catId: { $toObjectId: '$category' }, proId: { $toObjectId: '$promoter' } } },
    { $match: { isDraft: false, isPublic: true } },
    {
      $lookup: {
        from: 'categories',
        localField: 'catId',
        foreignField: '_id',
        as: 'ct',
        pipeline: [
          { $project: { name: 1 } }
        ]
      }
    },
    {
      $lookup: {
        from: 'promoters',
        localField: 'proId',
        foreignField: '_id',
        as: 'pro',
        pipeline: [
          { $project: { forename: 1 } }
        ]
      }
    }
  ]
  if (req.query.name) {
    filter[1].$match.$or = [
      { name: { $regex: new RegExp(req.query.name, 'i') } },
      { tags: { $regex: new RegExp(req.query.name, 'i') } }
    ]
  }
  const eventList = await this.Event.searchEvents(filter)
  return { events: eventList }
}

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 */
async function uploadImage (req, res) {
  const file = await req.body.cover
  const eventId = req.body._id.value
  await writeFile(join(__dirname, '..', '..', 'public', file.filename), await file.toBuffer())

  return await this.Event.updateEvent(eventId, req.user.id, { cover: `public/${file.filename}` })
}

module.exports = {
  createEvent,
  updateEvent,
  showEvents,
  findEvent,
  searchEvents,
  uploadImage
}
