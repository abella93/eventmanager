'use strict'

const {
  createEvent, updateEvent, showEvents,
  findEvent, searchEvents, uploadImage
} = require('./handlers')

const { sendEmails } = require('./hooks')
const { assertOwnership } = require('../../common/hooks')

const {
  createEventSchema, findEventsSchema, updateEventSchema,
  findEventSchema, searchEventSchema, uploadSchema
} = require('./schema')

/**
 * Gather all endpoints from the event resource
 * @param {import('fastify').FastifyInstance} fastify app
 * @param {Object} opts envionment options
 */
module.exports = async function (fastify, opts) {
  fastify.get('/', {
    preValidation: [fastify.authorize],
    schema: findEventsSchema
  }, showEvents)

  fastify.post('/', {
    preValidation: [fastify.authorize],
    onSend: [sendEmails],
    schema: createEventSchema
  }, createEvent)

  fastify.put('/:id', {
    preValidation: [fastify.authorize, assertOwnership],
    onSend: [sendEmails],
    schema: updateEventSchema
  }, updateEvent)

  fastify.get('/:id', findEventSchema, findEvent)

  fastify.get('/history', searchEventSchema, searchEvents)

  fastify.post('/upload', { preValidation: [fastify.authorize], schema: uploadSchema }, uploadImage)
}
