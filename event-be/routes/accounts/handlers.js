/**
 * Register an account into the system
 * @this {import('fastify').FastifyInstance } the istance of fastify
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 * @returns the created user
 */
async function signup (req, res) {
  try {
    return await this.Account.insertAccount(req.body)
  } catch (error) {
    return error.code === 11000
      ? res.badRequest('Account già presente')
      : res.internalServerError(error)
  }
}

/**
 * login an account into the system. Send 404 if the account not exist
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 * @returns the logged user
 */
async function signin (req, res) {
  const { username, password } = req.body
  const foundAccount = await this.Account.findAccountByCredentials(username, password)
  if (foundAccount) {
    const payload = { id: foundAccount._id.toString(), role: 'registeredUser' }
    const token = this.jwt.sign(payload)
    return { token }
  } else {
    return res.notFound('Account non trovato')
  }
}

/**
 * Get the role from the user
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 * @returns the logged user
 */
async function retrieveRole (req, res) {
  const token = req.headers.authorization.replace(/^Bearer\s+/, '')
  const payload = this.jwt.decode(token)
  return payload
}

module.exports = { signup, signin, retrieveRole }
