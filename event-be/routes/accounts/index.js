'use strict'

const { signup, signin, retrieveRole } = require('./handlers')
const { signupSchema, signinSchema, roleSchema } = require('./schema')

/**
 * Gather all endpoints from the account resource
 * @param {import('fastify').FastifyInstance} fastify app
 * @param {Object} opts envionment options
 */
module.exports = async function (fastify, opts) {
  fastify.post('/', signupSchema, signup)

  fastify.post('/auth', signinSchema, signin)

  fastify.post('/authorize', { preHandler: [fastify.authorize], schema: roleSchema }, retrieveRole)
}
