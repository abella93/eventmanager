const S = require('fluent-json-schema')
const { errorObject } = require('../../common/schemas')

const account = S.object()
  .additionalProperties(false)
  .prop('username', S.string().maxLength(50).required())
  .prop('password', S.string().minLength(8).maxLength(16).required())
  .prop('name', S.string().pattern('^[a-zA-Z]{2,20}$').required())
  .prop('email', S.string().format(S.FORMATS.EMAIL))

const signupSchema = {
  schema: {
    description: 'Create an account',
    tags: ['account'],
    body: account,
    response: {
      200: {
        type: 'object',
        properties: {
          _id: {
            type: 'string'
          }
        }
      },
      '4xx': errorObject,
      '5xx': errorObject
    }
  }
}

const token = S.object().prop('token', S.string())

const signinSchema = {
  schema: {
    description: 'Login an account if exist',
    tags: ['account'],
    body: account.only(['username', 'password']),
    response: {
      200: token,
      '4xx': errorObject
    }
  }
}

const roleSchema = {
  description: 'Get the role of the user',
  security: [
    { Bearer: [] }
  ],
  tags: ['account'],
  response: {
    200: S.object().prop('id', S.string()).prop('role', S.string()),
    '4xx': errorObject
  }
}

module.exports = { account, signupSchema, signinSchema, roleSchema }
