'use strict'

/**
 * Create a repository to access the account data model
 * @param {import('mongodb').MongoClient} storage the mongodb storage
 */
async function AccountRepository (storage) {
  /**
   * @type {import('mongodb').Collection} accounts
   */
  const accounts = storage.db.collection('accounts')

  await accounts.createIndex({ username: 1 }, { unique: true })

  return {

    /**
     * Create a new account
     * @param {Object} account the created account
     */
    async insertAccount (account) {
      const createdAccount = await accounts.insertOne(account)
      return { _id: createdAccount.insertedId.toString() }
    },

    /**
     * Find an account into the system by the submitted credentials
     * @param {string} username the username
     * @param {string} password the password
     * @returns The found account or null
     */
    async findAccountByCredentials (username, password) {
      return await accounts.findOne({ username: username, password: password })
    }
  }
}

module.exports = { AccountRepository }
