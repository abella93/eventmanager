/**
 * Check the ownership of a resource
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply} res
 */
async function assertOwnership (req, res) {
  if (req.body.owner && req.user.id !== req.body.owner) {
    return res.unauthorized('The resource is not yours')
  }
}

module.exports = { assertOwnership }
