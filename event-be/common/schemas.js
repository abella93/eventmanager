const S = require('fluent-json-schema')

const errorObject = S.object().id('#error')
  .prop('statusCode', S.number())
  .prop('error', S.string())
  .prop('message', S.string())

const updateResponse = S.object()
  .prop('count', S.number().required())

module.exports = { errorObject, updateResponse }
