import React from "react";
import PropTypes from "prop-types";
import { Stack, Typography } from "@mui/material";
import ResourceTable from "../resource/ResourceTable";
import { Add } from "@mui/icons-material";
import { Link } from "react-router-dom";

/**
 * Show a resource table page
 * @param { Object } props the props
 * @param { string } props.title
 * @param { string } props.resource
 * @param { Array } props.columns
 */
export default function ShowResources({ title, resource, columns }) {
  return (
    <Stack direction="column" spacing={2} width="100%">
      <Typography variant="h4" textAlign="center" marginTop="10px">
        {title}
        <sup>
          <Link to={`/dashboard/${resource}/new`}>
            <Add />
          </Link>
        </sup>
      </Typography>
      <ResourceTable resource={resource} columns={columns} />
    </Stack>
  );
}

ShowResources.propTypes = {
  title: PropTypes.string.isRequired,
  resource: PropTypes.string.isRequired,
};
