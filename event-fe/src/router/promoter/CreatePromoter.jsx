import { Button, LinearProgress, Stack, TextField, Typography } from '@mui/material';
import React from 'react'
import { Field, Form } from 'react-final-form';
import { useCreateResource } from "../../resource/ResourceHooks";
import Toast from '../../toast/Toast';
import { validateForm } from "../../form/validator";
import { promoter } from '../../form/promoter'
import { useAuthorization } from "../../account/AccountHooks";


export default function CreatePromoter() {
    const { data } = useAuthorization();
    
    const emptyPromoter = { forename: '', description: '', website: '', email: '', owner: data.data.id }
    const { isLoading, isError, isSuccess, mutate, error } = useCreateResource('promoters')
    const validatorHandler = function (values) {
        return validateForm(values, promoter())
    }
    return (
        <Stack direction="column" spacing={2} width="100%">
            {isLoading ? <LinearProgress /> : null}
            <Toast
                open={isSuccess || isError}
                type={isSuccess ? 'success' : 'error'}
                msg={error ? error.response.data.message : 'Salvataggio avvenuto con successo!'}
            />
            <Typography variant="h4" textAlign="center" marginTop="10px">Crea Organizzatore</Typography>
            <Form onSubmit={(values) => mutate(values)} initialValues={emptyPromoter} validate={validatorHandler}>
                {props => {
                    return (
                        <Stack component="form" spacing={3} padding="20px" onSubmit={props.handleSubmit}>
                            <Field name="forename">
                                {props =>
                                    <TextField
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        label="Nome"
                                        error={props.meta.touched && props.meta.error}
                                        helperText={props.meta.error}
                                    />
                                }
                            </Field>
                            <Field name="email">
                                {props =>
                                    <TextField
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        label="Email"
                                        type="email"
                                        error={props.meta.touched && props.meta.error}
                                        helperText={props.meta.error}
                                    />
                                }
                            </Field>
                            <Field name="description">
                                {props =>
                                    <TextField
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        label="Descrizione"
                                        error={props.meta.touched && props.meta.error}
                                        helperText={props.meta.error}
                                        multiline
                                        rows={4}
                                    />
                                }
                            </Field>
                            <Field name="website">
                                {props =>
                                    <TextField
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        label="Sito Web"
                                        type="url"
                                        error={props.meta.touched && props.meta.error}
                                        helperText={props.meta.error}
                                    />
                                }
                            </Field>

                            <Field name="owner">
                                {(props) => (
                                    <input
                                        type="hidden"
                                        name={props.input.name}
                                        value={props.input.value}
                                    />
                                )}
                            </Field>

                            <Button type="submit" variant="contained">Salva</Button>
                        </Stack>
                    )
                }}
            </Form>
        </Stack>
    )
}
