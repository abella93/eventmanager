import React from 'react'
import { useParams } from "react-router-dom"
import { useResource, useUpdateResource } from '../../resource/ResourceHooks'
import { Button, LinearProgress, Stack, TextField, Typography } from '@mui/material'
import Toast from '../../toast/Toast'
import { Field, Form } from 'react-final-form'
import { validateForm } from '../../form/validator'
import { promoter } from '../../form/promoter'

export default function UpdatePromoter() {
    const { id } = useParams()
    const { isLoading, data } = useResource('promoters', id)
    const mutator = useUpdateResource('promoters')
    const validatorHandler = function (values) {
        return validateForm(values, promoter())
    }
    return (
        <Stack direction="column" spacing={2} width="100%">
            {mutator.isLoading ? <LinearProgress /> : null}
            <Toast
                open={mutator.isSuccess || mutator.isError}
                type={mutator.isSuccess ? 'success' : 'error'}
                msg={mutator.error ? mutator.error.response.data.message : 'Salvataggio avvenuto con successo!'}
            />
            <Typography variant="h4" textAlign="center" marginTop="10px">Aggiorna Organizzatore</Typography>
            <Form onSubmit={(values) => mutator.mutate(values)} initialValues={isLoading ? {} : data.data} validate={validatorHandler}>
                {props => {
                    return (
                        <Stack component="form" spacing={3} padding="20px" onSubmit={props.handleSubmit}>
                            <Field name="forename">
                                {props =>
                                    <TextField
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        label="Nome"
                                        error={props.meta.touched && props.meta.error}
                                        helperText={props.meta.error}
                                    />
                                }
                            </Field>
                            <Field name="email">
                                {props =>
                                    <TextField
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        label="Email"
                                        type="email"
                                        error={props.meta.touched && props.meta.error}
                                        helperText={props.meta.error}
                                    />
                                }
                            </Field>
                            <Field name="description">
                                {props =>
                                    <TextField
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        label="Descrizione"
                                        error={props.meta.touched && props.meta.error}
                                        helperText={props.meta.error}
                                        multiline
                                        rows={4}
                                    />
                                }
                            </Field>
                            <Field name="website">
                                {props =>
                                    <TextField
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        label="Sito Web"
                                        type="url"
                                        error={props.meta.touched && props.meta.error}
                                        helperText={props.meta.error}
                                    />
                                }
                            </Field> 

                            <Field name="_id">
                                {props =>
                                    <input type="hidden" name={props.input.name} value={props.input.value} />
                                }
                            </Field>

                            <Field name="owner">
                                {(props) => (
                                    <input
                                        type="hidden"
                                        name={props.input.name}
                                        value={props.input.value}
                                    />
                                )}
                            </Field>

                            <Button type="submit" variant="contained">Salva</Button>
                        </Stack>
                    )
                }}
            </Form>
        </Stack>
    )
}
