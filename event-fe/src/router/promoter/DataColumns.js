import { Link } from "react-router-dom"
import { Edit } from "@mui/icons-material";

export const promoterColumns = [
    { field: 'id', headerName: 'ID'},
    { field: 'forename', headerName: 'Nome', flex: 1},
    { field: 'email', headerName: 'Email', flex: 1 },
    { field: '_id', headerName: 'Azioni', flex: 1, renderCell: function(params){
        return (
            <Link to={`/dashboard/promoters/${params.value}`}>
                <Edit />
            </Link>
        )
    }}
]