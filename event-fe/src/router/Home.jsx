import { Paper, TextField, Typography, useMediaQuery, useTheme } from "@mui/material";
import React, { createContext, useState } from "react";
import EventGrid from "../event/EventGrid";
import StickyFooter from "../styckyfooter/StickyFooter";
import { useDebouncedCallback } from "use-debounce";

export const EventContext = createContext();

export default function Home() {
  const [term, setTerm] = useState("");
  const debounced = useDebouncedCallback((val => setTerm(val)), 1000)
  const theme = useTheme()
  const isSm = useMediaQuery(theme.breakpoints.down('sm'))
  const handleChange = function (e) {
    debounced(e.target.value);
  };
  return (
    <StickyFooter>
      <Paper elevation={2} sx={{
        margin: '20px', padding: '20px', display: 'flex',
        justifyContent: 'space-around', alignItems: 'center'
      }}>
        {isSm ? null :
          <Typography variant="h3" color="primary">Ideato per chi ama fare</Typography>
        }
        <img style={{ maxWidth: '100%', height: 'auto' }} src="https://images.unsplash.com/photo-1517457373958-b7bdd4587205?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8ZXZlbnR8ZW58MHx8MHx8&auto=format&fit=crop&w=600&q=60" alt="Cover" />
      </Paper>
      <EventContext.Provider value={{ term, setTerm: debounced }}>
        <Paper elevation={2} sx={{
          margin: '20px', padding: '20px', 
          display: 'flex', justifyContent:"space-around", 
          flexWrap: "wrap"}}>
        <Typography variant="h5">Eventi attuali</Typography>
        <TextField
              fullWidth
              onChange={handleChange}
              placeholder="Cerca"
              variant="standard"
              size="small"
            />
        </Paper>
        <EventGrid />
      </EventContext.Provider>
    </StickyFooter>
  );
}
