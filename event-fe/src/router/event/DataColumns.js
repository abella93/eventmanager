import { Link } from "react-router-dom"
import { Edit } from "@mui/icons-material";
import { Typography } from "@mui/material";

export const eventColumns = [
    { field: 'id', headerName: 'ID'},
    { field: 'name', headerName: 'Nome', flex: 1},
    { field: 'isDraft', headerName: 'Bozza', flex: 1 },
    { field: 'participants', headerName: 'Partecipanti', flex: 1, renderCell: function(params){
        return <Typography>{params.value === -1 ? 'Nessun Limite' : params.value}</Typography>
    } },
    { field: 'starting', headerName: 'Data Inizio', flex: 1},
    { field: 'isOnline', headerName: 'Online', flex: 1},
    { field: 'isPublic', headerName: 'Pubblico', flex: 1},
    { field: '_id', headerName: 'Azioni', flex: 1, renderCell: function(params){
        return (
            <Link to={`/dashboard/events/${params.value}`}>
                <Edit />
            </Link>
        )
    }}
]