import React, { useRef } from "react";
import {
  useCreateResource,
  useResources,
  useUpload,
} from "../../resource/ResourceHooks";
import { useAuthorization } from "../../account/AccountHooks";
import {
  Stack,
  TextField,
  Typography,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  InputLabel,
  MenuItem,
  Select,
  LinearProgress,
  Button
} from "@mui/material";
import Toast from "../../toast/Toast";
import { Field, Form } from "react-final-form";
import DateAdapter from "@mui/lab/AdapterDayjs";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DateTimePicker from "@mui/lab/DateTimePicker";
import { validateForm } from "../../form/validator";
import { event } from "../../form/event";
import AddressInput from "../../address/AddressInput";

export default function CreateEvent() {
  const { data } = useAuthorization();
  const categories = useResources("categories");
  const promoters = useResources("promoters");
  const resourceMutator = useCreateResource("events");

  const uploadMutator = useUpload();
  const values = React.useMemo(
    () => ({
      name: "",
      description: "",
      place: "",
      starting: new Date(),
      duration: 0,
      category: null,
      promoter: null,
      participants: 0,
      isPublic: false,
      isOnline: false,
      isDraft: false,
      tags: [],
      addresses: [],
      owner: data.data.id,
    }),
    [data.data.id]
  );

  const validatorHandler = function (values) {
    return validateForm(values, event())
  }

  const cv = useRef(null);

  const handler = function (values) {
    resourceMutator.mutateAsync(values)
      .then(dt => {
        const fdata = new FormData();
        fdata.append("cover", cv.current.files[0]);
        fdata.append("_id", dt.data._id);
        uploadMutator.mutate(fdata);
      }).catch((error) => console.error(error))
  };

  return (
    <Stack direction="column" spacing={2} width="100%">
      {resourceMutator.isLoading ? <LinearProgress /> : null}
      <Toast
        open={resourceMutator.isSuccess || resourceMutator.isError}
        type={resourceMutator.isSuccess ? "success" : "error"}
        msg={
          resourceMutator.error
            ? resourceMutator.error.response.data.message
            : "Salvataggio avvenuto con successo!"
        }
      />
      <Typography variant="h4" textAlign="center" marginTop="10px">
        Crea Evento
      </Typography>
      <Form onSubmit={handler} initialValues={values} validate={validatorHandler}>
        {(prop) => {
          return (
            <Stack
              component="form"
              spacing={3}
              padding="20px"
              onSubmit={prop.handleSubmit}
            >
              <Field name="name">
                {(props) => (
                  <TextField
                    label="Nome"
                    name={props.input.name}
                    value={props.input.value}
                    onChange={props.input.onChange}
                    error={props.meta.error && props.meta.touched}
                    helperText={props.meta.error}
                  />
                )}
              </Field>
              <Field name="description">
                {(props) => (
                  <TextField
                    label="Descrizione"
                    name={props.input.name}
                    value={props.input.value}
                    onChange={props.input.onChange}
                    error={props.meta.error && props.meta.touched}
                    helperText={props.meta.error}
                    multiline
                    rows={4}
                  />
                )}
              </Field>
              <Field name="link">
                {(props) => (
                  <TextField
                    type="url"
                    label="Link"
                    name={props.input.name}
                    value={props.input.value}
                    onChange={props.input.onChange}
                    error={props.meta.error && props.meta.touched}
                    helperText={props.meta.error}
                  />
                )}
              </Field>

              <Field name="place">
                {(props) => (
                  <AddressInput {...props}/>
                )}
              </Field>
              
              <Field name="starting">
                {(props) => (
                  <LocalizationProvider dateAdapter={DateAdapter}>
                    <DateTimePicker
                      label="Inizio"
                      onChange={props.input.onChange}
                      value={props.input.value}
                      name={props.input.name}
                      error={props.meta.error && props.meta.touched}
                      disablePast
                      renderInput={(params) => <TextField {...params} />}
                    />
                  </LocalizationProvider>
                )}
              </Field>
              <Field name="duration" parse={val => Number(val)}>
                {(props) => (
                  <TextField
                    type="number"
                    name={props.input.name}
                    onChange={props.input.onChange}
                    value={props.input.value}
                    error={props.meta.error && props.meta.touched}
                    helperText={props.meta.error}
                    label="Durata - (In Ore)"
                  />
                )}
              </Field>
              <Field name="category">
                {(props) => (
                  <FormControl fullWidth>
                    <InputLabel id="category">Categoria</InputLabel>
                    <Select
                      labelId="category"
                      id="cateogry"
                      value={props.input.value}
                      label="Categoria"
                      name={props.input.name}
                      error={props.meta.error && props.meta.touched}
                      onChange={props.input.onChange}
                    >
                      {categories.data ? (
                        categories.data.data.categories.map((cat) => (
                          <MenuItem key={cat._id} value={cat._id}>
                            {cat.name}
                          </MenuItem>
                        ))
                      ) : (
                        <MenuItem key={-1} value={-1}>
                          Nessuna Categoria Trovata
                        </MenuItem>
                      )}
                    </Select>
                  </FormControl>
                )}
              </Field>
              <Field name="promoter">
                {(props) => (
                  <FormControl fullWidth>
                    <InputLabel id="promoter">Organizzatore</InputLabel>
                    <Select
                      labelId="promoter"
                      id="promoter"
                      value={props.input.value}
                      label="Organizzatore"
                      name={props.input.name}
                      error={props.meta.error && props.meta.touched}
                      onChange={props.input.onChange}
                    >
                      {promoters.data ? (
                        promoters.data.data.promoters.map((prom) => (
                          <MenuItem key={prom._id} value={prom._id}>
                            {prom.forename}
                          </MenuItem>
                        ))
                      ) : (
                        <MenuItem key={-1} value={-1}>
                          Nessun Organizzatore
                        </MenuItem>
                      )}
                    </Select>
                  </FormControl>
                )}
              </Field>
              <Field name="isPublic" type="checkbox">
                {(props) => (
                  <FormGroup>
                    <FormControlLabel
                      control={
                        <Checkbox
                          name={props.input.name}
                          onChange={props.input.onChange}
                          value={props.input.value}
                          checked={props.input.checked}
                          error={props.meta.error && props.meta.touched}
                        />
                      }
                      label="Evento Pubblico"
                    />
                  </FormGroup>
                )}
              </Field>

              <Field name="isOnline" type="checkbox">
                {(props) => (
                  <FormGroup>
                    <FormControlLabel
                      control={
                        <Checkbox
                          name={props.input.name}
                          onChange={props.input.onChange}
                          value={props.input.value}
                          checked={props.input.checked}
                          error={props.meta.error && props.meta.touched}
                        />
                      }
                      label="Evento Online"
                    />
                  </FormGroup>
                )}
              </Field>

              <Field name="isDraft" type="checkbox">
                {(props) => (
                  <FormGroup>
                    <FormControlLabel
                      control={
                        <Checkbox
                          name={props.input.name}
                          onChange={props.input.onChange}
                          value={props.input.value}
                          checked={props.input.checked}
                          error={props.meta.error && props.meta.touched}
                        />
                      }
                      label="Salva come Bozza"
                    />
                  </FormGroup>
                )}
              </Field>

              <Field
                name="tags"
                parse={(val) => val.split(",")}
                format={(val) => (val ? val.join(",") : "")}
              >
                {(props) => (
                  <TextField
                    label="Tags"
                    name={props.input.name}
                    value={props.input.value}
                    onChange={props.input.onChange}
                    error={props.meta.error && props.meta.touched}
                    helperText={props.meta.error}

                  />
                )}
              </Field>

              <Field name="owner">
                {(props) => (
                  <input
                    type="hidden"
                    name={props.input.name}
                    value={props.input.value}
                  />
                )}
              </Field>

              <Field 
                name="addresses"
                parse={(val) => val.split(",")}
                format={(val) => (val ? val.join(",") : "")}
              >
                {(props) => (
                    <TextField
                      disabled={prop.values.isPublic}
                      value={props.input.value}
                      label="Invita altri utenti"
                      name={props.input.name}
                      error={props.meta.error && props.meta.touched}
                      onChange={props.input.onChange}
                      helperText={props.meta.error}
                    />
                )}
              </Field>

              <Field name="participants" parse={val => Number(val)}>
                {(props) => (
                    <TextField
                      type='number'
                      disabled={!prop.values.isPublic}
                      value={props.input.value}
                      label="Partecipanti"
                      name={props.input.name}
                      error={props.meta.error && props.meta.touched}
                      onChange={props.input.onChange}
                      helperText={props.meta.error}
                    />
                )}
              </Field>

              <Field name="cover">
                {(props) => (
                  <input ref={cv} type="file" name={props.input.name} />
                )}
              </Field>

              <Button type="submit" variant="contained">
                Salva
              </Button>
            </Stack>
          );
        }}
      </Form>
    </Stack>
  );
}
