import { Button, LinearProgress, Stack, TextField, Typography } from '@mui/material'
import React from 'react'
import { Field, Form } from 'react-final-form'
import { useCreateResource } from '../../resource/ResourceHooks'
import Toast from '../../toast/Toast'
import { category } from "../../form/category"
import { validateForm } from "../../form/validator";
import { useAuthorization } from "../../account/AccountHooks";


export default function CreateCategory() {

    const { data } = useAuthorization();

    const emptyCategory = { name: '', description: '', slug: '',  owner: data.data.id }

    const { isLoading, isError, isSuccess, mutate, error } = useCreateResource('categories')

    const validator = function (values) {
        return validateForm(values, category())
    }

    return (
        <Stack direction="column" spacing={2} width="100%">
            {isLoading ? <LinearProgress /> : null}
            <Toast
                open={isSuccess || isError}
                type={isSuccess ? 'success' : 'error'}
                msg={error ? error.response.data.message : 'Salvataggio avvenuto con successo!'}
            />
            <Typography variant="h4" textAlign="center" marginTop="10px">Crea Categoria</Typography>
            <Form onSubmit={(values) => mutate(values)} initialValues={emptyCategory} validate={validator}>
                {props => {
                    return (
                        <Stack component="form" spacing={3} padding="20px" onSubmit={props.handleSubmit}>
                            <Field name="name">
                                {props =>
                                    <TextField
                                        label="Nome"
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        error={props.meta.error && props.meta.touched}
                                        helperText={props.meta.error}
                                    />
                                }
                            </Field>
                            <Field name="description">
                                {props =>
                                    <TextField
                                        label="Descrizione"
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        error={props.meta.error && props.meta.touched}
                                        helperText={props.meta.error}
                                        multiline
                                        rows={4}
                                    />
                                }
                            </Field>
                            <Field name="slug">
                                {props =>
                                    <TextField
                                        label="Slug"
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        error={props.meta.error && props.meta.touched}
                                        helperText={props.meta.error}
                                    />
                                }
                            </Field>


                            <Field name="owner">
                                {(props) => (
                                    <input
                                        type="hidden"
                                        name={props.input.name}
                                        value={props.input.value}
                                    />
                                )}
                            </Field>

                            <Button type="submit" variant="contained">Salva</Button>
                        </Stack>
                    )
                }}
            </Form>
        </Stack>
    )
}
