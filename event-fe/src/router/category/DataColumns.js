import { Link } from "react-router-dom"
import { Edit } from "@mui/icons-material";

export const categoryColumns = [
    { field: 'id', headerName: 'ID'},
    { field: 'name', headerName: 'Nome', flex: 1},
    { field: 'slug', headerName: 'Slug', flex: 1 },
    { field: '_id', headerName: 'Azioni', flex: 1, renderCell: function(params){
        return (
            <Link to={`/dashboard/categories/${params.value}`}>
                <Edit />
            </Link>
        )
    }}
]