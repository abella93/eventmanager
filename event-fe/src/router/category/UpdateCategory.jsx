import React from 'react'
import { useParams } from 'react-router';
import { useResource, useUpdateResource } from '../../resource/ResourceHooks'
import { Button, LinearProgress, Stack, TextField, Typography } from '@mui/material'
import Toast from '../../toast/Toast'
import { Field, Form } from 'react-final-form'
import { validateForm } from '../../form/validator';
import { category } from '../../form/category';


export default function UpdateCategory() {
    const { id } = useParams()
    const { isLoading, data } = useResource('categories', id)
    const mutator = useUpdateResource('categories')

    const validateHandler = function(values) {
        return validateForm(values, category())
    }
    return (
        <Stack direction="column" spacing={2} width="100%">
            {mutator.isLoading ? <LinearProgress /> : null}
            <Toast
                open={mutator.isSuccess || mutator.isError}
                type={mutator.isSuccess ? 'success' : 'error'}
                msg={mutator.error ? mutator.error.response.data.message : 'Salvataggio avvenuto con successo!'}
            />
            <Typography variant="h4" textAlign="center" marginTop="10px">Aggiorna Categoria</Typography>
            <Form onSubmit={(values) => mutator.mutate(values)} initialValues={isLoading ? {} : data.data} validate={validateHandler}>
                {props => {
                    return (
                        <Stack component="form" spacing={3} padding="20px" onSubmit={props.handleSubmit}>
                            <Field name="name">
                                {props =>
                                    <TextField
                                        label="Nome"
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        error={props.meta.error && props.meta.touched}
                                        helperText={props.meta.error}
                                    />
                                }
                            </Field>
                            <Field name="description">
                                {props =>
                                    <TextField
                                        label="Descrizione"
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        error={props.meta.error && props.meta.touched}
                                        helperText={props.meta.error}
                                        multiline
                                        rows={4}
                                    />
                                }
                            </Field>
                            <Field name="slug">
                                {props =>
                                    <TextField
                                        label="Slug"
                                        name={props.input.name}
                                        value={props.input.value}
                                        onChange={props.input.onChange}
                                        error={props.meta.error && props.meta.touched}
                                        helperText={props.meta.error}
                                    />
                                }
                            </Field>

                            <Field name="_id">
                                {props => 
                                    <input type="hidden" name={props.input.name} value={props.input.value} />
                                }
                            </Field>

                            <Field name="owner">
                                {(props) => (
                                    <input
                                        type="hidden"
                                        name={props.input.name}
                                        value={props.input.value}
                                    />
                                )}
                            </Field>
                            
                            <Button type="submit" variant="contained">Salva</Button>
                        </Stack>
                    )
                }}
            </Form>
        </Stack>
    )
}
