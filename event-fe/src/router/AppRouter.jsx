import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Signup from "../account/Signup";
import Home from "./Home";
import NotFound from "./NotFound";
import Signin from "../account/Signin";
import Dashboard from "../dashboard/Dashboard";
import ShowResources from "./ShowResources";
import CreateCategory from "./category/CreateCategory";
import UpdateCategory from "./category/UpdateCategory";
import CreatePromoter from "./promoter/CreatePromoter";
import UpdatePromoter from "./promoter/UpdatePromoter";
import CreateEvent from "./event/CreateEvent";
import UpdateEvent from "./event/UpdateEvent";
import { eventColumns } from "./event/DataColumns";
import { categoryColumns } from "./category/DataColumns";
import { promoterColumns } from "./promoter/DataColumns";

export default function AppRouter() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="*" element={<NotFound />} />
        <Route path="/accounts">
          <Route path="login" element={<Signin />} />
          <Route path="new" element={<Signup />} />
        </Route>
        <Route path="/dashboard" element={<Dashboard />}>
          <Route path="categories">
            <Route
              path=""
              element={
                <ShowResources
                  title="Gestisci Categorie"
                  resource="categories"
                  columns={categoryColumns}
                />
              }
            />
            <Route path="new" element={<CreateCategory />} />
            <Route path=":id" element={<UpdateCategory />} />
          </Route>
          <Route path="promoters">
            <Route
              path=""
              element={
                <ShowResources
                  title="Gestisci Organizzatori"
                  resource="promoters"
                  columns={promoterColumns}
                />
              }
            />
            <Route path="new" element={<CreatePromoter />} />
            <Route path=":id" element={<UpdatePromoter />} />
          </Route>
          <Route path="events">
            <Route
              path=""
              element={
                <ShowResources title="Gestisci Eventi" resource="events" columns={eventColumns} />
              }
            />
            <Route path="new" element={<CreateEvent />} />
            <Route path=":id" element={<UpdateEvent />} />
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}
