import React from "react";
import { Box, Divider, Link, Paper, Typography } from "@mui/material";
import { Lock } from "@mui/icons-material";
import { red } from "@mui/material/colors";

export default function Unauthorized() {

  return (
    <Box
      width="100vw"
      height="100vh"
      display="flex"
      justifyContent="center"
      alignItems="center"
      backgroundColor={red[300]}
    >
      <Paper
        elevation={0}
        sx={{ textAlign: "center", backgroundColor: red[300] }}
      >
        <Typography variant="h2" color="white">
          <Lock fontSize="50px" />
          <Divider />
          Non sei autorizzato!
        </Typography>
        <Link href="/accounts/login" variant="h3" color="#fff" marginTop='10px'>Torna alla Login</Link>
      </Paper>
    </Box>
  );
}
