import { Box, Paper, Typography } from "@mui/material";
import { orange } from "@mui/material/colors";
import React from "react";
import { useLocation } from "react-router";
import { Link } from "react-router-dom";

export default function NotFound() {
  const location = useLocation();
  return (
    <Box
      width="100vw"
      height="100vh"
      display="flex"
      justifyContent="center"
      alignItems="center"
      backgroundColor={orange[100]}
    >
      <Paper
        elevation={0}
        sx={{ textAlign: "center", backgroundColor: orange[100] }}
      >
        <Typography variant="h4">
          La pagina: {location.pathname} che stai cercando non è stata trovata.
        </Typography>
        <Typography variant="h4">
            <Link to="/">Torna alla homepage</Link>
        </Typography>
      </Paper>
    </Box>
  );
}
