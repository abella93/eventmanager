import { nominatim } from "../client"

export function getDetails(address) {
    return nominatim.get('/search', { params: { q: address, format: 'json' }})
}