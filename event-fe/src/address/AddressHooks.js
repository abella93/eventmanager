import { getDetails } from "./api";
import { useQuery } from "react-query";

export function useAddress(address) {
    return useQuery(['Get-address', address], async () => await getDetails(address))
}