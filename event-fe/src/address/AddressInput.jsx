import { Autocomplete, TextField } from '@mui/material'
import React, { useState } from 'react'
import { useAddress } from './AddressHooks'
import { useDebouncedCallback } from "use-debounce";

/**
 * 
 * @param {import('react-final-form').FieldProps} props 
 * @returns 
 */
export default function AddressInput(props) {
    const [address, setAddress] = useState(props.input.value)
    const debounced = useDebouncedCallback(val => setAddress(val), 1000)
    const { data, isSuccess, isLoading } = useAddress(address)
    function handleChange(e) {
        debounced(e.target.value)
        props.input.onChange(e)
    }
    return (
        <>
            <Autocomplete
                disablePortal
                id="address-complete"
                value={props.input.value}
                name={props.input.name}
                onChange={(ev, opt) => props.input.onChange(opt)}
                options={isSuccess ? data.data.map(dt => dt.display_name) : []}
                loading={isLoading}
                freeSolo
                autoSelect
                renderInput={(params) =>
                    <TextField
                        {...params}
                        label="Luogo"
                        onChange={handleChange}
                        error={props.meta.error && props.meta.touched}
                        helperText={props.meta.error}
                    />}
            />
        </>
    )
}
