import React from 'react'
import { useAddress } from './AddressHooks'
import { MapContainer, TileLayer, Marker } from "react-leaflet";
/**
 * Get the map
 * @param {Object} props properties
 * @param {string} props.address
 * @returns The map
 */
export default function AddressMap(props) {
    const { data, isLoading, isError } = useAddress(props.address)
    if (isLoading) {
        return <div>Loading</div>
    } else if (isError) {
        return <div>Errore</div>
    } else {
        return (
            data.data[0] ?
            <MapContainer style={{height: '500px', width: '500px'}} center={[data.data[0].lat, data.data[0].lon]} zoom={13} scrollWheelZoom={false}>
                <TileLayer attribution='© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={[data.data[0].lat, data.data[0].lon]}/>
            </MapContainer>
            : <div>Mappa non disponibile</div>
        )
    }
}
