import { client } from "../client";

export function createResource(name, payload) {
  return client.post(`${name}`, payload);
}

export function readResources(name) {
  return client.get(`/${name}`);
}

export function updateResource(name, payload) {
  const id = payload._id;
  delete payload._id;
  return client.put(`${name}/${id}`, payload);
}

export function readResource(name, id) {
  return client.get(`${name}/${id}`);
}

export function searchResource(name, filters) {
  return client.get(`${name}/history`, { params: filters });
}

export function uploadImage(formData) {
  return client.post("events/upload", formData);
}
