import { useMutation, useQuery } from "react-query";
import {
  readResources,
  readResource,
  createResource,
  updateResource,
  searchResource,
  uploadImage,
} from "./api";

/**
 * Create a custom hook starting by a resource name
 * @param {string} resourceName the name of the resource
 */
export function useResources(resourceName) {
  return useQuery(
    `Get-${resourceName}`,
    async () => await readResources(resourceName)
  );
}

export function useResource(resourceName, id) {
  return useQuery(
    [`Find-${resourceName}`, id],
    async () => await readResource(resourceName, id)
  );
}

export function useCreateResource(resourceName) {
  return useMutation(
    async (payload) => await createResource(resourceName, payload)
  );
}

export function useUpdateResource(resourceName) {
  return useMutation(
    async (payload) => await updateResource(resourceName, payload)
  );
}

export function useSearchResource(resourceName, filters) {
  return useQuery(
    [`Search-${resourceName}`, filters],
    async () => await searchResource(resourceName, filters)
  );
}

export function useUpload() {
  return useMutation(async (formData) => await uploadImage(formData));
}
