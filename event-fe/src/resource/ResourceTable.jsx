import React from "react";
import PropTypes from "prop-types";
import { DataGrid } from "@mui/x-data-grid";
import { Typography } from "@mui/material";
import { useResources } from "./ResourceHooks";

function createRows(data) {
  return data.map((dt, index) => {
    dt.id = index;
    return dt;
  });
}

/**
 * Create a resource table
 * @param {Object} props the props
 * @param { string } props.resource The resource name
 * @param { Array } props.columns rhe columns
 * @returns The table
 */
export default function ResourceTable({ resource, columns }) {
  const { isLoading, data, isSuccess } = useResources(resource);
  return !isSuccess ? (
    <Typography variant="h2">Non ci sono elementi al momento</Typography>
  ) : (
    <DataGrid
      columns={columns}
      rows={createRows(data.data[resource])}
      loading={isLoading}
    />
  );
}

ResourceTable.propTypes = {
  resource: PropTypes.string.isRequired,
};
