import { client } from "../client"

export function login(credentials) {
    return client.post('accounts/auth', credentials)
}

export function authorize() {
    return client.post('accounts/authorize')
}

export function signup(account) {
    return client.post('accounts', account)
}

export function fetchEmails() {
    return client.get('accounts')
}