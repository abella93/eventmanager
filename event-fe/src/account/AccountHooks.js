import { useMutation, useQuery } from "react-query"
import { authorize, fetchEmails, login, signup } from "./api"
import { client } from "../client"
import { useNavigate } from "react-router-dom";

export function useLogin() {
    const navigate = useNavigate()
    return useMutation((credentials) => login(credentials), {
        retry: false,
        onSuccess: (data, variables, context) => {
            client.defaults.headers.common['Authorization'] = `Bearer ${data.data.token}`
            navigate('/dashboard/categories')
          }
    })
}

export function useAuthorization() {
    return useQuery('authorize', async () => await authorize(), { retry: false })
}

export function useSignup() {
    const navigate = useNavigate()
    return useMutation(async (payload) => await signup(payload), {
        onSuccess: (data, variables, context) => {
            navigate('/accounts/login')
        }
    })
}

export function useFetchAccounts() {
    return useQuery('Fetch-Accounts', async () => await fetchEmails())
}