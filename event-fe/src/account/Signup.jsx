import React from "react";
import SplitScreen from "../splitscreen/SplitScreen";
import { orange } from "@mui/material/colors";
import { Button, Stack, Link, TextField, Typography, LinearProgress } from "@mui/material";
import { useSignup } from "./AccountHooks";
import Toast from "../toast/Toast";
import { Field, Form } from "react-final-form";
import { signup } from "../form/account";
import { validateForm } from "../form/validator";
import { Link as RouterLink } from "react-router-dom";

export default function Signup() {
  const mutator = useSignup();
  return (
    <SplitScreen>
      <SplitScreen.Left sx={{ flexGrow: 1 }}>
        <Form
        
          onSubmit={(values) => mutator.mutate(values)}
          validate={(values) => validateForm(values, signup())}
        >
          {(props) => (
            <Stack
              component="form"
              spacing={2}
              onSubmit={e => props.handleSubmit(e)}
              width="50%"
              padding="15px"
              textAlign="center"
            >
              {mutator.isLoading ? <LinearProgress /> : null}
              <Toast
                open={mutator.isError}
                type="error"
                msg={mutator.error ? mutator.error.response.data.message : ""}
              />
              <Link 
                to="/" 
                color="primary" 
                component={RouterLink} 
                sx={{ fontFamily: "Roboto", fontSize: "2rem" }} 
                underline="hover">
                  EventUI
              </Link>
              <Typography variant="h3">Crea un account</Typography>
              <Field name="username">
                {(props) => (
                  <TextField
                    id="username-field"
                    label="Username"
                    variant="outlined"
                    name={props.input.name}
                    value={props.input.value}
                    onChange={props.input.onChange}
                    error={props.meta.error && props.meta.touched}
                    helperText={JSON.stringify(props.meta.error)}
                  />
                )}
              </Field>
              <Field name="email">
                {(props) => (
                  <TextField
                    id="email-field"
                    label="Email"
                    variant="outlined"
                    type="email"
                    name={props.input.name}
                    value={props.input.value}
                    onChange={props.input.onChange}
                    error={props.meta.error && props.meta.touched}
                    helperText={JSON.stringify(props.meta.error)}
                  />
                )}
              </Field>
              <Field name="name">
                {(props) => (
                  <TextField
                    id="name-field"
                    label="Nome"
                    variant="outlined"
                    name={props.input.name}
                    value={props.input.value}
                    onChange={props.input.onChange}
                    error={props.meta.error && props.meta.touched}
                    helperText={JSON.stringify(props.meta.error)}
                  />
                )}
              </Field>

              <Field name="password">
                {(props) => (
                  <TextField
                    id="password-field"
                    label="Password"
                    variant="outlined"
                    type="password"
                    name={props.input.name}
                    value={props.input.value}
                    onChange={props.input.onChange}
                    error={props.meta.error && props.meta.touched}
                    helperText={JSON.stringify(props.meta.error)}
                  />
                )}
              </Field>

              <Button type="submit" variant="contained">
                Registrati
              </Button>
            </Stack>
          )}
        </Form>
      </SplitScreen.Left>
      <SplitScreen.Right sx={{ backgroundColor: orange[100], width: "50%" }} />
    </SplitScreen>
  );
}
