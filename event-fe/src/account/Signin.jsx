import React from "react";
import SplitScreen from "../splitscreen/SplitScreen";
import { orange } from "@mui/material/colors";
import {
  Button,
  LinearProgress,
  Link,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { Link as RouterLink } from "react-router-dom";
import { Field, Form } from "react-final-form";
import { useLogin } from "./AccountHooks";
import Toast from "../toast/Toast";
import { validateForm } from "../form/validator";
import { signin } from "../form/account";

export default function Signin() {
  const mutator = useLogin();
  return (
    <SplitScreen>
      <SplitScreen.Left sx={{ flexGrow: 1 }}>
        <Form
          onSubmit={(values) => mutator.mutate(values)}
          validate={(values) => validateForm(values, signin())}
        >
          {(props) => (
            <Stack
              onSubmit={props.handleSubmit}
              component="form"
              spacing={2}
              width="50%"
              padding="15px"
              textAlign="center"
            >
              {mutator.isLoading ? <LinearProgress /> : null}
              <Toast
                open={mutator.isError}
                type="error"
                msg={mutator.error ? mutator.error.response.data.message : ""}
              />
              <Link 
                to="/" 
                color="primary" 
                component={RouterLink} 
                sx={{ fontFamily: "Roboto", fontSize: "2rem" }} 
                underline="hover">
                  EventUI
              </Link>
              <Typography variant="h3">Accedi</Typography>
              <Field name="username">
                {(props) => (
                  <TextField
                    id="username-field"
                    name={props.input.name}
                    label="Username"
                    variant="outlined"
                    value={props.input.value}
                    error={props.meta.error && props.meta.touched}
                    helperText={JSON.stringify(props.meta.error)}
                    onChange={props.input.onChange}
                  />
                )}
              </Field>
              <Field name="password">
                {(props) => (
                  <TextField
                    name={props.input.name}
                    value={props.input.value}
                    onChange={props.input.onChange}
                    id="password-field"
                    label="Password"
                    variant="outlined"
                    error={props.meta.error && props.meta.touched}
                    helperText={JSON.stringify(props.meta.error)}
                    type="password"
                  />
                )}
              </Field>
              <Button type="submit" variant="contained">
                Accedi
              </Button>
              <Link
                component={RouterLink}
                sx={{ fontFamily: "Roboto" }}
                underline="hover"
                to="/accounts/new"
                color="black"
              >
                Registrati
              </Link>
            </Stack>
          )}
        </Form>
      </SplitScreen.Left>
      <SplitScreen.Right sx={{ backgroundColor: orange[100], width: "50%" }} />
    </SplitScreen>
  );
}
