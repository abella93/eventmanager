export function event(){
    return {
        name: {
            type: 'string',
            presence: true
        },
        description: {},
        category: {
            presence: true,
            type: 'string'
        },
        owner: {
            presence: true,
            type: 'string'
        },
        duration: {
            numericality: true
        },
        promoter: {
            presence: true,
            type: 'string'
        },
        link: {
            url: true
        },
        addresses: {
            listOf: 'email'
        },
        participants: {
            numericality: true
        }
    }
}