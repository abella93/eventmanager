export function promoter(){
    return {
        forename: {
            type: 'string',
            presence: true,
            length: {
                maximum: 30
            }
        },
        email: {
            email: true,
            presence: true
        },
        description: {
            type: 'string',
            length: {
                maximum: 255
            }
        },
        website: {
            url: true
        }
    }
}