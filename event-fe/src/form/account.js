export function signin() {
    return {
        username: {
            presence: true,
            length: {
                maximum: 50
            }
        },
        password: {
            presence: true,
            length: {
                minimum: 8,
                maximum: 16
            }
        }
    }
}

export function signup(){
    const base = signin()
    base.email = {
        presence: true,
        email: true
    }
    base.name = {
        presence: true
    }
    return base
}