export function category() {
  return {
    name: {
      type: "string",
      presence: true,
    },
    slug: {
        type: 'string'
    },
    description: {
        type: 'string',
        length: {
            maximum: 255
        }
    },
    icon: {
        url: true
    }
  };
}
