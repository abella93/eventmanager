import validate from "validate.js";

validate.validators.listOf = function(value, options, key, attributes){
  if(!options) {
    throw new Error('Type options is required!')
  }

  if(!validate.isArray(value)){
    return `${key} must be a list`
  } else if(value.length === 0) {
    return undefined
  } else {
    for(let i=0; i < value.length; i++) {
      const res = validate({i: value[i]}, { i: { email: true }})
      if(res !== undefined) {
        return `${value} not respect type ${options}`
      }
    }
    return undefined
  }
}

export function validateForm(values, schema) {
  return validate(values, schema);
}
