import React, { useState } from "react";
import { EventType } from "./EventType";
import {
  Card,
  CardContent,
  CardMedia,
  Typography,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  CardActions,
  Button,
  Dialog,
  DialogTitle,
  Popover,
  Link,
  DialogContent,
  DialogActions,
} from "@mui/material";
import {
  AccessTime,
  Category,
  MyLocation,
  People,
  TagRounded
} from "@mui/icons-material";
import "dayjs/locale/it"
import dayjs from "dayjs";
import AddressMap from "../address/AddressMap";
/**
 *
 * @param {Object} props
 * @param { import('./EventType').EventType } props.event
 */
export default function EventCard({ event }) {
  const [open, setOpen] = useState(false)
  const [anchor, setAnchor] = useState(null)
  
  
  return (
    <Card sx={{ maxWidth: "350px" }}>
      <CardMedia
        component="img"
        height="140"
        image={event.cover ? `${process.env.REACT_APP_API}${event.cover}` : "/logo.png"}
        alt="Prova"
      />
      <CardContent>
        <Link href={event.link} sx={{wordWrap: 'break-word'}}>{event.name}</Link>
        <List>
          <ListItem>
            <ListItemIcon>
              <Category />
            </ListItemIcon>
            <ListItemText primary={event.ct[0].name} />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <AccessTime />
            </ListItemIcon>
            <ListItemText primary={event.starting ? dayjs(event.starting).locale('it').format('LLL') : null} />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <MyLocation />
            </ListItemIcon>
            <ListItemText primary={event.place} />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <People />
            </ListItemIcon>
            <ListItemText primary={event.participants === 0 ? 'Nessun Limite' : event.participants} />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <TagRounded/>
            </ListItemIcon>
            <ListItemText 
              sx={{cursor: 'pointer'}} 
              primary="Mostra Tag" 
              onClick={(ev) => setAnchor(ev.currentTarget)} />
            <Popover anchorEl={anchor} open={Boolean(anchor)} onClose={() => setAnchor(null)}>
              <Typography padding="10px">{event.tags.join(',')}</Typography>
            </Popover>
          </ListItem>
        </List>
      </CardContent>
      <CardActions>
        <Button onClick={(ev) => setOpen(true)} size="small">Dettagli</Button>
      </CardActions>
    <Dialog onClose={() => setOpen(false)} open={open}>
      <DialogTitle>Dettagli Evento</DialogTitle>
      <DialogContent>
      <AddressMap address={event.place}/>
      <List>
        <ListItem>
          <ListItemText primary={`Inizio: ${event.starting ? dayjs(event.starting).locale('it').format('LLL') : null}`}/>
        </ListItem>
        <ListItem>
          <ListItemText primary={`Durata (In - Ore): ${event.duration}`}/>
        </ListItem>
        <ListItem>
          <ListItemText primary={`Organizzatore: ${event.pro[0].forename}`}/>
        </ListItem>
        <ListItem>
          <ListItemText primary={`Descrizione: ${event.description || 'Nessuna descrizione'}`}/>
        </ListItem>
      </List>
      </DialogContent>
      <DialogActions>
          <Button onClick={(e) => setOpen(false)}>Chiudi</Button>
      </DialogActions>
    </Dialog>
    </Card>
  );
}

EventCard.propTypes = EventType;
