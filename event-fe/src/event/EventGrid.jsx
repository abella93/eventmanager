import {
  Typography,
  Grid,
  LinearProgress,
  Box,
} from "@mui/material";
import React, { useContext } from "react";
import { useSearchResource } from "../resource/ResourceHooks";
import { EventContext } from "../router/Home";
import EventCard from "./EventCard";

/**
 * Create an event Grid
 * @param {Object} props
 * @returns the grid
 */
export default function EventGrid() {
  const { term } = useContext(EventContext);
  const { data, isLoading, isError } = useSearchResource("events", {
    name: term,
  });
  if (isError) {
    return <Typography variant="h3">Non ci sono eventi al momento</Typography>;
  } else {
    return (
      <Box padding="20px" maxWidth="100%">
        <Grid
          container
          spacing={1}
          justifyContent="space-evenly"
        >
          {isLoading ? (
            <LinearProgress />
          ) : (
            data.data.events.map((ev) => {
              return (
                <Grid item key={ev._id}>
                  <EventCard event={ev} />
                </Grid>
              );
            })
          )}
        </Grid>
      </Box>
    );
  }
}
