import PropTypes from "prop-types";

export const EventType = {
  event: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    tags: PropTypes.arrayOf(PropTypes.string),
    starting: PropTypes.string,
    place: PropTypes.string,
    participants: PropTypes.number,
    isPublic: PropTypes.bool,
  }),
};
