import {
  AppBar,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Stack,
  Toolbar,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from "react";
import {
  Menu,
  Home,
  Category,
  Event,
  Person,
  ArrowLeft,
  Logout,
} from "@mui/icons-material";
import { Outlet, useNavigate } from "react-router-dom";
import Unauthorized from "../router/Unauthorized";
import { useAuthorization } from "../account/AccountHooks";
import { client } from "../client";


export default function Dashboard() {
  const [open, setOpen] = useState(false);

  const navigate = useNavigate();

  const openMenu = function () {
    setOpen(true);
  };

  const closeMenu = function () {
    setOpen(false);
  };

  const signout = function() {
    navigate("/accounts/login")
    delete client.defaults.headers.common['Authorization']
  }

  const { isSuccess } = useAuthorization();
  if (isSuccess) {
    return (
      <Stack direction="column" open={open}>
        <AppBar position="relative">
          <Toolbar>
            <IconButton onClick={() => navigate(-1)}>
              <ArrowLeft />
            </IconButton>
            <Typography
              variant="h6"
              noWrap
              sx={{ flexGrow: 1 }}
              component="div"
            >
              Dashboard Utente
            </Typography>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="end"
              onClick={openMenu}
            >
              <Menu />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Box display="flex" flexDirection="row" minHeight="100vh">
          <Drawer open={open} anchor="left" onClose={closeMenu}>
            <List>
              <ListItem>
                <ListItemButton>
                  <ListItemIcon
                    onClick={() => navigate("/dashboard/categories")}
                  >
                    <Home />
                  </ListItemIcon>
                  <ListItemText primary="Home" />
                </ListItemButton>
              </ListItem>
              <ListItem>
                <ListItemButton
                  onClick={() => navigate("/dashboard/categories")}
                >
                  <ListItemIcon>
                    <Category />
                  </ListItemIcon>
                  <ListItemText primary="Gestisci Categorie" />
                </ListItemButton>
              </ListItem>
              <ListItem>
                <ListItemButton onClick={() => navigate("/dashboard/events")}>
                  <ListItemIcon>
                    <Event />
                  </ListItemIcon>
                  <ListItemText primary="Gestisci Eventi" />
                </ListItemButton>
              </ListItem>
              <ListItem>
                <ListItemButton
                  onClick={() => navigate("/dashboard/promoters")}
                >
                  <ListItemIcon>
                    <Person />
                  </ListItemIcon>
                  <ListItemText primary="Gestisci Organizzatori" />
                </ListItemButton>
              </ListItem>
              <ListItem>
                <ListItemButton onClick={() => signout()}>
                  <ListItemIcon>
                    <Logout />
                  </ListItemIcon>
                  <ListItemText primary="Logout" />
                </ListItemButton>
              </ListItem>
            </List>
          </Drawer>
          <Outlet />
        </Box>
      </Stack>
    );
  } else {
    return <Unauthorized />;
  }
}
