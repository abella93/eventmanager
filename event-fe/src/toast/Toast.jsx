import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Alert, Snackbar } from "@mui/material";

/**
 *
 * @param { Object } props
 * @param { string } props.type the type of toast
 * @param { string } props.msg the message of the toast
 * @param { bool } props.open the state
 */
export default function Toast({ type, msg, open }) {
  const [visible, setVisible] = useState(open);
  function handleClose(){
    setVisible(false)
  }
  useEffect(() => {
    setVisible(open)
  }, [open])

  return (
    <Snackbar
      open={visible}
      autoHideDuration={4000}
      onClose={handleClose}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
    >
      <Alert onClose={handleClose} severity={type}>
        {msg}
      </Alert>
    </Snackbar>
  );
}

Toast.propTypes = {
  type: PropTypes.string.isRequired,
  msg: PropTypes.string.isRequired,
  open: PropTypes.bool,
};
