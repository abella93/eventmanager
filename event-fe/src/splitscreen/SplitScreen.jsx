import { Box } from "@mui/system";
import React from "react";

function allowedChildren(children) {
  const filteredChildren = React.Children.map(children, function (ch) {
    if (ch.type.name === "Left" || ch.type.name === "Right") {
      return ch;
    }
  });
  return filteredChildren;
}

export default function SplitScreen({ children }) {
  return (
    <Box sx={{ display: "flex", flexDirection: "row", minHeight: "100vh" }}>
      {allowedChildren(children)}
    </Box>
  );
}

function Right({ children, ...other }) {
  return <Box {...other}> {children} </Box>;
}

function Left({ children }) {
  return children;
}

SplitScreen.Right = Right;

SplitScreen.Left = Left;
