import {
  AppBar,
  Divider,
  Toolbar,
  Typography,
  Link
} from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import PropTypes from "prop-types";
import { Link as RouterLink } from "react-router-dom";

export default function Header({ brand = "LOGO" }) {

  return (
    <Box component="nav" sx={{ maxWidth: "100%" }}>
      <AppBar color="transparent" position="relative">
        <Toolbar
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Box sx={{ display: "inline-flex", alignItems: "center" }}>
            <Typography
              variant="h6"
              color="primary"
              sx={{ marginRight: "10px" }}
            >
              {brand}
            </Typography>
          </Box>
          <Box sx={{ display: "inline-flex", alignItems: "center" }}>
            <Link
              component={RouterLink}
              sx={{ fontFamily: "Roboto" }}
              underline="hover"
              to="/dashboard/events/new"
              color="black"
            >
              Crea Evento
            </Link>
            <Divider orientation="vertical" flexItem sx={{ mx: "5px" }} />
            <Link
              component={RouterLink}
              sx={{ fontFamily: "Roboto" }}
              underline="hover"
              color="black"
              to="/accounts/login"
            >
              Accedi
            </Link>
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

Header.propTypes = {
  brand: PropTypes.string,
};
