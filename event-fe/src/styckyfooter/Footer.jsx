import { Box } from "@mui/system";
import React from "react";
import { deepPurple } from "@mui/material/colors";
import { List, ListItem, ListItemText } from "@mui/material";
import { useMediaQuery } from "@mui/material";
import { useTheme } from "@mui/material/styles";

export default function Footer() {
  const theme = useTheme();
  const isLg = useMediaQuery(theme.breakpoints.up("lg"));
  const responsiveList = isLg ? { display: "inline-flex" } : null;
  return (
    <Box
      component="footer"
      sx={{
        backgroundColor: deepPurple[900],
        color: "white",
        padding: "10px",
      }}
    >
      <List sx={responsiveList}>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>© 2021 EventUI</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Come funziona</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Prezzi</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Contatta l'assistenza</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Informazioni</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Blog</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Aiuto</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Protezione</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Sviluppatori</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Termini</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Privacy</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Accessibilità</ListItemText>
        </ListItem>
        <ListItem sx={{ textAlign: "center" }}>
          <ListItemText>Cookie</ListItemText>
        </ListItem>
      </List>
    </Box>
  );
}
