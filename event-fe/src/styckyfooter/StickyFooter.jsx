import React from "react";
import Header from "./Header";
import { Box } from "@mui/system";
import Footer from "./Footer";

export default function StickyFooter({ children }) {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        flexWrap: "wrap",
        minHeight: "100vh",
        maxWidth: "100%",
      }}
    >
      <Header brand="EventUI" />
      <Box component="main" sx={{ flexGrow: 1 }}>
        {children}
      </Box>
      <Footer />
    </Box>
  );
}
