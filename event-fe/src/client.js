import axios from "axios"

const client = axios.create({
    baseURL: `${process.env.REACT_APP_API}`
})

const nominatim = axios.create({
    baseURL: 'https://nominatim.openstreetmap.org'
})

export { client, nominatim }
